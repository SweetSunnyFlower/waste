FROM golang:latest
ENV GOPROXY https://goproxy.cn,direct
WORKDIR /var/www/sweetsunnyflower
COPY . /var/www/sweetsunnyflower
RUN go build .
EXPOSE 3000
ENTRYPOINT ["./sweetsunnyflower"]