package bootstrap

import (
	"embed"
	"net/http"
	"strings"
	"sweetsunnyflower/app/http/middlewares"
	"sweetsunnyflower/routes"

	"html/template"
	"sweetsunnyflower/pkg/config"
	"sweetsunnyflower/pkg/console"
	"sweetsunnyflower/pkg/websocket"

	"github.com/gin-gonic/gin"
)

func Boot(f embed.FS) {
	// 设置 gin 的运行模式，支持 debug, release, test
	// release 会屏蔽调试信息，官方建议生产环境中使用
	// 非 release 模式 gin 终端打印太多信息，干扰到我们程序中的 Log
	// 故此设置为 release，有特殊情况手动改为 debug 即可
	gin.SetMode(gin.ReleaseMode)

	r := gin.New()

	Route(r, f)
	console.Success("服务启动中,监听端口:" + config.Get("app.port") + "，请访问 http://127.0.0.1:" + config.Get("app.port"))
	if err := r.Run(":" + config.Get("app.port")); err != nil {
		console.Error("启动服务失败:" + err.Error())
	}
}

// 注册路由
func Route(route *gin.Engine, f embed.FS) {

	// 注册全局路由
	regisgerGlobalMiddlewares(route)

	go websocket.NewClientManager().Start()

	// 注册websocket路由
	routes.RegisterWebsocketRoutes(route)

	// 注册路由
	routes.RegisterApiRoutes(route)

	// 注册静态资源
	RegisterStaticResources(route, f)

	// 注册web路由
	routes.RegisterWebRoutes(route, f)

	// 注册404处理器
	setup404Handler(route)
}

func RegisterStaticResources(route *gin.Engine, f embed.FS) {
	templ := template.Must(template.New("").ParseFS(f, "templates/*"))
	route.SetHTMLTemplate(templ)

	// example: /resources/assets/images/example.png
	route.StaticFS("/resources", http.FS(f))

	route.Static("/public", "public")
}

func regisgerGlobalMiddlewares(route *gin.Engine) {
	route.Use(middlewares.Cors(), middlewares.Logger(), middlewares.Recovery())
}

func setup404Handler(router *gin.Engine) {
	// 处理 404 请求
	router.NoRoute(func(c *gin.Context) {
		// 获取标头信息的 Accept 信息
		acceptString := c.Request.Header.Get("Accept")
		if strings.Contains(acceptString, "text/html") {
			// 如果是 HTML 的话
			c.String(http.StatusNotFound, "页面返回 404")
		} else {
			// 默认返回 JSON
			c.JSON(http.StatusNotFound, gin.H{
				"error_code":    404,
				"error_message": "路由未定义，请确认 url 和请求方法是否正确。",
			})
		}
	})
}
