package bootstrap

import (
	"sweetsunnyflower/app/queues"
	"sweetsunnyflower/pkg/queue"
)

// https://github.com/hibiken/asynq
func Queue() {
	queue := queue.NewQueue()
	for name, fn := range queues.Tasks {
		queue.Register(name, fn)
	}
	queue.Start()
}
