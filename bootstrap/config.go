package bootstrap

import (
	"flag"
	"sweetsunnyflower/config"
	pkgConfig "sweetsunnyflower/pkg/config"
)

func LoadConfig() {
	config.Initialize()

	// 配置初始化，依赖命令行 --env 参数
	var env string
	flag.StringVar(&env, "env", "", "加载 .env 文件，如 --env=testing 加载的是 .env.testing 文件")
	flag.Parse()

	pkgConfig.InitConfig(env)
}
