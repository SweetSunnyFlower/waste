package bootstrap

import (
	"sweetsunnyflower/app/schedules"
	"sweetsunnyflower/pkg/schedule"
)

func Schedule() {
	schedule := schedule.NewCron()

	schedules.Register()

	schedule.Start()
}
