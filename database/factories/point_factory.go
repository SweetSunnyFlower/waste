package factories

import (
    "sweetsunnyflower/app/models/point"
    // "sweetsunnyflower/pkg/helpers"

    // "github.com/bxcodec/faker/v3"
)

func MakePoints(count int) []point.Point {

    var objs []point.Point

    // 设置唯一性，如 Point 模型的某个字段需要唯一，即可取消注释
    // faker.SetGenerateUniqueValues(true)

    for i := 0; i < count; i++ {
        pointModel := point.Point{
            // FIXME()
        }
        objs = append(objs, pointModel)
    }

    return objs
}