package factories

import (
	"sweetsunnyflower/app/models/address"
	// "sweetsunnyflower/pkg/helpers"
	// "github.com/bxcodec/faker/v3"
)

func MakeAddresses(count int) []address.Address {

	var objs []address.Address

	// 设置唯一性，如 Address 模型的某个字段需要唯一，即可取消注释
	// faker.SetGenerateUniqueValues(true)
	addresses := []string{
		"大同",
		"新荣",
		"阳高",
		"浑源",
		"右玉",
		"怀仁",
		"左云",
	}
	for i := 0; i < count; i++ {

		addressModel := address.Address{
			Name: addresses[i],
		}
		objs = append(objs, addressModel)
	}

	return objs
}
