package factories

import (
	"sweetsunnyflower/app/models/user_passenger"
)

func MakeUserPassengers(count int) []user_passenger.UserPassenger {

	var objs []user_passenger.UserPassenger

	// 设置唯一性，如 UserPassenger 模型的某个字段需要唯一，即可取消注释
	// faker.SetGenerateUniqueValues(true)

	for i := 0; i < count; i++ {
		userPassengerModel := user_passenger.UserPassenger{
			// FIXME()
		}
		objs = append(objs, userPassengerModel)
	}

	return objs
}
