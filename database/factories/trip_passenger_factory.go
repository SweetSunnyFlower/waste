package factories

import (
    "sweetsunnyflower/app/models/trip_passenger"
    // "sweetsunnyflower/pkg/helpers"

    // "github.com/bxcodec/faker/v3"
)

func MakeTripPassengers(count int) []trip_passenger.TripPassenger {

    var objs []trip_passenger.TripPassenger

    // 设置唯一性，如 TripPassenger 模型的某个字段需要唯一，即可取消注释
    // faker.SetGenerateUniqueValues(true)

    for i := 0; i < count; i++ {
        tripPassengerModel := trip_passenger.TripPassenger{
            // FIXME()
        }
        objs = append(objs, tripPassengerModel)
    }

    return objs
}