package factories

import (
	"sweetsunnyflower/app/models/user_car"

	"github.com/bxcodec/faker/v3"
)

func MakeUserCars(count int) []user_car.UserCar {

	var objs []user_car.UserCar

	// 设置唯一性，如 UserCar 模型的某个字段需要唯一，即可取消注释
	// faker.SetGenerateUniqueValues(true)

	for i := 0; i < count; i++ {
		userCarModel := user_car.UserCar{
			UserID:   1,
			Brand:    faker.ChineseName(),
			Color:    faker.DomainName(),
			Plate:    faker.DomainName(),
			Sites:    4,
			UseYears: 1,
		}
		objs = append(objs, userCarModel)
	}

	return objs
}
