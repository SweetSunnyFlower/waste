package factories

import (
	"sweetsunnyflower/app/models/user_follow"
)

func MakeUserFollows(count int) []user_follow.UserFollow {

	var objs []user_follow.UserFollow

	// 设置唯一性，如 UserFollow 模型的某个字段需要唯一，即可取消注释
	// faker.SetGenerateUniqueValues(true)

	for i := 0; i < count; i++ {
		userFollowModel := user_follow.UserFollow{
			// FIXME()
			UserID:   1,
			FollowID: 2,
		}
		objs = append(objs, userFollowModel)
	}

	return objs
}
