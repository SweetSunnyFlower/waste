package factories

import (
    "sweetsunnyflower/app/models/suggest"
    // "sweetsunnyflower/pkg/helpers"

    // "github.com/bxcodec/faker/v3"
)

func MakeSuggests(count int) []suggest.Suggest {

    var objs []suggest.Suggest

    // 设置唯一性，如 Suggest 模型的某个字段需要唯一，即可取消注释
    // faker.SetGenerateUniqueValues(true)

    for i := 0; i < count; i++ {
        suggestModel := suggest.Suggest{
            // FIXME()
        }
        objs = append(objs, suggestModel)
    }

    return objs
}