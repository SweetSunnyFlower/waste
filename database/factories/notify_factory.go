package factories

import (
	"sweetsunnyflower/app/models/notify"
	// "sweetsunnyflower/pkg/helpers"
	// "github.com/bxcodec/faker/v3"
)

func MakeNotifies(count int) []notify.Notify {

	var objs []notify.Notify

	// 设置唯一性，如 Notify 模型的某个字段需要唯一，即可取消注释
	// faker.SetGenerateUniqueValues(true)

	content := &notify.Content{
		Title:   "实名认证通知",
		Message: "实名认证通过获得1000积分",
		Target:  "/users/certification",
	}

	c, _ := content.Marshal()

	for i := 0; i < count; i++ {
		notifyModel := notify.Notify{
			UserID:  1,
			Type:    notify.Certification,
			Status:  1,
			Content: string(c),
		}
		objs = append(objs, notifyModel)
	}

	return objs
}
