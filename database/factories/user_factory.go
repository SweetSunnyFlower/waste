// Package factories 存放工厂方法
package factories

import (
	"sweetsunnyflower/app/models/user"
	"sweetsunnyflower/pkg/helpers"

	"github.com/bxcodec/faker/v3"
)

func MakeUsers(times int) []user.User {

	var objs []user.User

	// 设置唯一值
	faker.SetGenerateUniqueValues(true)

	for i := 0; i < times; i++ {
		model := user.User{
			OpenId:        helpers.RandomNumber(11),
			Name:          faker.Username(),
			Email:         faker.Email(),
			Phone:         helpers.RandomNumber(11),
			Password:      "$2a$14$oPzVkIdwJ8KqY0erYAYQxOuAAlbI/sFIsH0C0R4MPc.3JbWWSuaUe",
			Avatar:        "https://picsum.photos/200",
			Gender:        1,
			Certification: 1,
			Balance:       0,
			Point:         100,
			Work:          "同煤集团",
		}
		objs = append(objs, model)
	}

	return objs
}
