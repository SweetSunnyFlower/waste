package factories

import (
	"sweetsunnyflower/app/models/user_trip"
	// "sweetsunnyflower/pkg/helpers"
	// "github.com/bxcodec/faker/v3"
)

func MakeUserTrips(count int) []user_trip.UserTrip {

	var objs []user_trip.UserTrip

	// 设置唯一性，如 UserTrip 模型的某个字段需要唯一，即可取消注释
	// faker.SetGenerateUniqueValues(true)

	for i := 0; i < count; i++ {
		userTripModel := user_trip.UserTrip{
			UserID:  2,
			StartID: 1,
			EndID:   2,
			Time:    "2024-01-24 12:00:00",
			CarId:   23,
			Sites:   1,
			Price:   10,
			Status:  1,
			Remark:  "啊啊啊",
		}
		objs = append(objs, userTripModel)
	}

	return objs
}
