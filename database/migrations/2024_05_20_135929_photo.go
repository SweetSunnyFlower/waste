package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type Photo struct {
		models.Model
		Name      string `gorm:"type:varchar(100);comment:名称;not null;" json:"name"`
		OriginURL string `gorm:"type:varchar(1000);comment:原始图片;not null;" json:"origin_url"`
		PsURL     string `gorm:"type:varchar(1000);comment:图片地址;not null;" json:"ps_url"`
		Remark    string `gorm:"type:varchar(1000);comment:备注;not null;" json:"remark"`
		DemoURL   string `gorm:"type:varchar(1000);comment:demo地址;not null;" json:"demo_url"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&Photo{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&Photo{})
	}

	migrate.Add("2024_05_20_135929_photo", up, down)
}
