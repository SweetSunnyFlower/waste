package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type TripPassenger struct {
		models.Model

		TripID    uint64 `gorm:"column:trip_id;comment:行程ID;index;" json:"trip_id"`
		UserID    uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
		Status    uint8  `gorm:"column:status;comment:状态;default:1;" json:"status"`
		Passenger string `gorm:"type:text;column:passenger;comment:乘客信息;" json:"passenger"`
		Trip      string `gorm:"type:text;column:trip;comment:行程信息;" json:"trip"`
		Booking   string `gorm:"type:text;column:booking;comment:预约信息;" json:"booking"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&TripPassenger{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&TripPassenger{})
	}

	migrate.Add("2024_01_24_144429_trip_passenger", up, down)
}
