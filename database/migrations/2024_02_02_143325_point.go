package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type Point struct {
		models.Model

		UserID      uint64 `gorm:"column:user_id;not null"`
		Type        uint   `gorm:"column:type;not null"`
		Action      uint   `gorm:"column:action;not null"`    // 加还是减
		TargetID    uint64 `gorm:"column:target_id;not null"` // 关联ID
		Value       int    `gorm:"column:value;not null"`
		Description string `gorm:"column:description;not null"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&Point{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&Point{})
	}

	migrate.Add("2024_02_02_143325_point", up, down)
}
