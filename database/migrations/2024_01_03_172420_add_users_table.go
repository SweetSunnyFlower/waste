package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type User struct {
		models.Model

		OpenId        string `gorm:"type:varchar(255);comment:微信openid;not null;index" json:"openid"`
		Name          string `gorm:"type:varchar(255);comment:用户真实姓名;not null;index" json:"name"`
		Nickname      string `gorm:"type:varchar(255);comment:用户昵称;not null" json:"nickname"`
		Avatar        string `gorm:"type:varchar(255);comment:用户头像;not null;" json:"avatar"`
		Email         string `gorm:"type:varchar(255);comment:邮箱;index;default:null" json:"email"`
		Phone         string `gorm:"type:varchar(20);comment:手机号;index;default:null" json:"phone"`
		Gender        uint8  `gorm:"comment:性别 2:女,1:男;default:0;" db:"gender" json:"gender" form:"gender"`
		Age           string `gorm:"type:varchar(255);comment:年龄;default:null" json:"age"`
		Work          string `gorm:"type:varchar(255);comment:工作;default:null" json:"work"`
		Certification uint8  `gorm:"type:tinyint;comment:是否认证;default:null" json:"certification"`
		Balance       uint64 `gorm:"comment:余额;default:0" json:"balance"`
		Point         uint64 `gorm:"comment:积分;default:0" json:"point"`
		Status        uint8  `gorm:"comment:状态 1:正常,0:禁用;default:1" json:"status"`
		IsAdmin       uint8  `gorm:"comment:是否为管理员;default:0" json:"is_admin"`
		Follows       uint64 `gorm:"comment:关注数;default:0" json:"follows"`
		Blacks        uint64 `gorm:"comment:拉黑数;default:0" json:"blacks"`

		Password string `gorm:"type:varchar(255)" json:"-"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&User{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&User{})
	}

	migrate.Add("2024_01_03_172420_add_users_table", up, down)
}
