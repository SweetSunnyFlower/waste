package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type Sign struct {
		models.Model

		UserID uint64 `gorm:"column:user_id;not null"`
		Week   int    `gorm:"column:week;not null"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&Sign{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&Sign{})
	}

	migrate.Add("2024_02_02_140605_sign", up, down)
}
