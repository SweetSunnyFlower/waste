package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type UserTrip struct {
		models.Model

		UserID    uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
		StartID   uint64 `gorm:"column:start_id;comment:起点ID;index;" json:"start_id"`
		EndID     uint64 `gorm:"column:end_id;comment:终点ID;index;" json:"end_id"`
		Time      string `gorm:"column:time;comment:起点时间;index;" json:"time"`
		CarId     uint64 `gorm:"column:car_id;comment:车辆ID;index;" json:"car_id"`
		Sites     uint   `gorm:"column:sites;comment:座位数量;index;" json:"sites"`
		Remaining uint   `gorm:"column:remaining;comment:剩余座位;index;" json:"remaining"`
		Price     uint   `gorm:"column:price;comment:价格;index;" json:"price"`
		Status    uint   `gorm:"column:status;comment:状态;index;" json:"status"`
		Remark    string `gorm:"column:remark;comment:备注;" json:"remark"`
		Line      string `gorm:"column:line;comment:线路;" json:"line"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&UserTrip{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&UserTrip{})
	}

	migrate.Add("2024_01_23_161859_user_trip", up, down)
}
