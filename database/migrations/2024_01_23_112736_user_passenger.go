package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type UserPassenger struct {
		models.Model

		UserID uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
		Name   string `gorm:"column:name;comment:姓名;index;" json:"name"`
		Phone  string `gorm:"column:phone;comment:手机号;index;" json:"phone"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&UserPassenger{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&UserPassenger{})
	}

	migrate.Add("2024_01_23_112736_user_passenger", up, down)
}
