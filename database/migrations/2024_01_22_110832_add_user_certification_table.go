package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type UserCertification struct {
		models.Model
		UserID      uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
		Name        string `gorm:"column:name;comment:姓名;index;" json:"name"`
		IdCard      string `gorm:"column:id_card;comment:身份证号;index;" json:"id_card"`
		IdCardFront string `gorm:"column:id_card_front;comment:身份证正面照;index;" json:"id_card_front"`
		IdCardBack  string `gorm:"column:id_card_back;comment:身份证反面照;index;" json:"id_card_back"`
		Status      uint8  `gorm:"column:status;comment:状态;index;" json:"status"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&UserCertification{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&UserCertification{})
	}

	migrate.Add("2024_01_22_110832_add_user_certification_table", up, down)
}
