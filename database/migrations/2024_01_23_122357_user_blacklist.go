package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type UserBlacklist struct {
		models.Model

		UserID  uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
		BlackID uint64 `gorm:"column:black_id;comment:黑名单用户ID;index;" json:"black_id"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&UserBlacklist{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&UserBlacklist{})
	}

	migrate.Add("2024_01_23_122357_user_blacklist", up, down)
}
