package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type Address struct {
		models.Model

		Name string `gorm:"type:varchar(255)" db:"name" json:"name" form:"name"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&Address{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&Address{})
	}

	migrate.Add("2024_01_23_165520_address", up, down)
}
