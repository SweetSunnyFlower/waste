package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type Suggest struct {
		models.Model

		UserID  uint64 `gorm:"column:user_id;not null"`
		Type    string `gorm:"column:type;not null"`
		Suggest string `gorm:"column:suggest;not null"`
		Status  int    `gorm:"column:status;not null;default:0"`
		Contact string `gorm:"column:contact;not null;default:''"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&Suggest{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&Suggest{})
	}

	migrate.Add("2024_02_06_112159_suggest", up, down)
}
