package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type Chat struct {
		models.Model

		UserID  uint64 `gorm:"column:user_id;not null"`
		Type    string `gorm:"column:type;not null"`
		Content string `gorm:"column:content;not null"`
		ToID    uint64 `gorm:"column:to_id;not null"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&Chat{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&Chat{})
	}

	migrate.Add("2024_02_07_122432_chat", up, down)
}
