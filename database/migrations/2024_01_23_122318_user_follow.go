package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type UserFollow struct {
		models.Model

		UserID   uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
		FollowID uint64 `gorm:"column:follow_id;comment:关注用户ID;index;" json:"follow_id"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&UserFollow{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&UserFollow{})
	}

	migrate.Add("2024_01_23_122318_user_follow", up, down)
}
