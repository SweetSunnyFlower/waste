package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"
	"time"

	"gorm.io/gorm"
)

func init() {

	type VerifyCode struct {
		models.Model

		Key        string        `gorm:"type:varchar(255);comment:key;not null;index" db:"key" json:"key"`
		Code       string        `gorm:"type:varchar(255);comment:验证码;not null;index" db:"code" json:"code"`
		Phone      string        `gorm:"type:varchar(20);comment:手机号;index;default:null"`
		Verify     uint8         `gorm:"comment:是否验证 0:否1:是;default:0;" db:"verify" json:"verify" form:"verify"`
		ExpireTime time.Duration `gorm:"comment:过期时间" json:"expire_time" db:"expire_time"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&VerifyCode{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&VerifyCode{})
	}

	migrate.Add("2024_01_05_111202_add_verify_code_table", up, down)
}
