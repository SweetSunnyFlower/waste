package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type Notify struct {
		models.Model

		UserID  uint64 `gorm:"column:user_id;comment:用户id;index;" db:"user_id;index;" json:"user_id" form:"user_id"`
		Type    uint64 `gorm:"column:type;" db:"type" json:"type"`
		Content string `gorm:"column:content;" db:"content" json:"content"`
		Status  uint8  `gorm:"column:status;index;" db:"status" json:"status"`
		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&Notify{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&Notify{})
	}

	migrate.Add("2024_01_28_122404_notify", up, down)
}
