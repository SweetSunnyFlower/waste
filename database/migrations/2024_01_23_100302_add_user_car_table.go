package migrations

import (
	"database/sql"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/migrate"

	"gorm.io/gorm"
)

func init() {

	type UserCar struct {
		models.Model

		Brand    string `gorm:"column:brand;type:varchar(255);comment:品牌;" json:"brand"`
		Color    string `gorm:"column:color;type:varchar(255);comment:颜色;" json:"color"`
		Plate    string `gorm:"column:plate;type:varchar(255);comment:车牌;" json:"plate"`
		Sites    uint   `gorm:"column:sites;type:int(10);comment:座位数;" json:"sites"`
		UserID   uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
		UseYears uint   `gorm:"column:use_years;type:int(10);comment:使用年限;" json:"use_years"`

		models.CommonTimestampsField
	}

	up := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.AutoMigrate(&UserCar{})
	}

	down := func(migrator gorm.Migrator, DB *sql.DB) {
		migrator.DropTable(&UserCar{})
	}

	migrate.Add("2024_01_23_100302_add_user_car_table", up, down)
}
