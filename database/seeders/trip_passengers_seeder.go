package seeders

import (
    "fmt"
    "sweetsunnyflower/database/factories"
    "sweetsunnyflower/pkg/console"
    "sweetsunnyflower/pkg/logger"
    "sweetsunnyflower/pkg/seed"

    "gorm.io/gorm"
)

func init() {

    seed.Add("SeedTripPassengersTable", func(db *gorm.DB) {

        tripPassengers  := factories.MakeTripPassengers(10)

        result := db.Table("trip_passengers").Create(&tripPassengers)

        if err := result.Error; err != nil {
            logger.LogIf(err)
            return
        }

        console.Success(fmt.Sprintf("Table [%v] %v rows seeded", result.Statement.Table, result.RowsAffected))
    })
}