package seeders

import (
    "fmt"
    "sweetsunnyflower/database/factories"
    "sweetsunnyflower/pkg/console"
    "sweetsunnyflower/pkg/logger"
    "sweetsunnyflower/pkg/seed"

    "gorm.io/gorm"
)

func init() {

    seed.Add("SeedUserPassengersTable", func(db *gorm.DB) {

        userPassengers  := factories.MakeUserPassengers(10)

        result := db.Table("user_passengers").Create(&userPassengers)

        if err := result.Error; err != nil {
            logger.LogIf(err)
            return
        }

        console.Success(fmt.Sprintf("Table [%v] %v rows seeded", result.Statement.Table, result.RowsAffected))
    })
}