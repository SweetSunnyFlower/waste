package seeders

import (
	"fmt"
	"sweetsunnyflower/database/factories"
	"sweetsunnyflower/pkg/console"
	"sweetsunnyflower/pkg/logger"
	"sweetsunnyflower/pkg/seed"

	"gorm.io/gorm"
)

func init() {

	seed.Add("SeedUserTripsTable", func(db *gorm.DB) {

		userTrips := factories.MakeUserTrips(100)

		result := db.Table("user_trips").Create(&userTrips)

		if err := result.Error; err != nil {
			logger.LogIf(err)
			return
		}

		console.Success(fmt.Sprintf("Table [%v] %v rows seeded", result.Statement.Table, result.RowsAffected))
	})
}
