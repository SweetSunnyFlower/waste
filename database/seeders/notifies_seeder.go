package seeders

import (
	"fmt"
	"sweetsunnyflower/database/factories"
	"sweetsunnyflower/pkg/console"
	"sweetsunnyflower/pkg/logger"
	"sweetsunnyflower/pkg/seed"

	"gorm.io/gorm"
)

func init() {

	seed.Add("SeedNotifiesTable", func(db *gorm.DB) {

		notifies := factories.MakeNotifies(2)

		result := db.Table("notifies").Create(&notifies)

		if err := result.Error; err != nil {
			logger.LogIf(err)
			return
		}

		console.Success(fmt.Sprintf("Table [%v] %v rows seeded", result.Statement.Table, result.RowsAffected))
	})
}
