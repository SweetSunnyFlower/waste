package seeders

import (
	"fmt"
	"sweetsunnyflower/database/factories"
	"sweetsunnyflower/pkg/console"
	"sweetsunnyflower/pkg/logger"
	"sweetsunnyflower/pkg/seed"

	"gorm.io/gorm"
)

func init() {

	seed.Add("SeedAddressesTable", func(db *gorm.DB) {

		addresses := factories.MakeAddresses(6)

		result := db.Table("addresses").Create(&addresses)

		if err := result.Error; err != nil {
			logger.LogIf(err)
			return
		}

		console.Success(fmt.Sprintf("Table [%v] %v rows seeded", result.Statement.Table, result.RowsAffected))
	})
}
