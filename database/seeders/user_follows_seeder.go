package seeders

import (
	"fmt"
	"sweetsunnyflower/database/factories"
	"sweetsunnyflower/pkg/console"
	"sweetsunnyflower/pkg/logger"
	"sweetsunnyflower/pkg/seed"

	"gorm.io/gorm"
)

func init() {

	seed.Add("SeedUserFollowsTable", func(db *gorm.DB) {

		userFollows := factories.MakeUserFollows(1)

		result := db.Table("user_follows").Create(&userFollows)

		if err := result.Error; err != nil {
			logger.LogIf(err)
			return
		}

		console.Success(fmt.Sprintf("Table [%v] %v rows seeded", result.Statement.Table, result.RowsAffected))
	})
}
