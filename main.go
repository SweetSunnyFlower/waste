package main

import (
	"embed"
	"fmt"
	"os"
	"sweetsunnyflower/app/cmd"

	"sweetsunnyflower/app/cmd/make"
	"sweetsunnyflower/bootstrap"
	"sweetsunnyflower/pkg/config"
	"sweetsunnyflower/pkg/console"

	"github.com/spf13/cobra"
)

//go:embed assets/* templates/*
var f embed.FS

func main() {

	// 应用的主入口，默认调用 cmd.CmdServe 命令
	var rootCmd = &cobra.Command{
		Use:   "Sweetsunnyflower",
		Short: "A simple forum project",
		Long:  `Default will run "serve" command, you can use "-h" flag to see all subcommands`,

		// rootCmd 的所有子命令都会执行以下代码
		PersistentPreRun: func(command *cobra.Command, args []string) {

			// 配置初始化，依赖命令行 --env 参数
			config.InitConfig(cmd.Env)

			// 加载配置文件
			bootstrap.LoadConfig()

			// 初始化日志
			bootstrap.Logger()

			// 初始化数据库
			bootstrap.DB()

			// 初始化Redis
			bootstrap.Redis()

			// 初始化缓存
			bootstrap.Cache()
		},
	}

	// 注册子命令
	rootCmd.AddCommand(
		CmdServe,
		cmd.CmdKey,
		cmd.CmdPlay,
		make.CmdMake,
		cmd.CmdMigrate,
		cmd.CmdDBSeed,
		cmd.CmdCache,
		cmd.CmdUsertoken,
	)

	// 配置默认运行 Web 服务
	cmd.RegisterDefaultCmd(rootCmd, CmdServe)

	// 注册全局参数，--env
	cmd.RegisterGlobalFlags(rootCmd)

	// 执行主命令
	if err := rootCmd.Execute(); err != nil {
		console.Exit(fmt.Sprintf("Failed to run app with %v: %s", os.Args, err.Error()))
	}
}

// CmdServe represents the available web sub-command.
var CmdServe = &cobra.Command{
	Use:   "serve",
	Short: "Start web server",
	Run:   runWeb,
	Args:  cobra.NoArgs,
}

func runWeb(cmd *cobra.Command, args []string) {

	// 定时任务
	// go bootstrap.Schedule()

	// 队列服务
	// go bootstrap.Queue()

	// 启动服务
	bootstrap.Boot(f)
}
