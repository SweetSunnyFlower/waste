// Package config 站点配置信息
package config

import "sweetsunnyflower/pkg/config"

func init() {
	config.Add("wechat", func() map[string]interface{} {
		return map[string]interface{}{

			// 默认是阿里云的测试 sign_name 和 template_code
			"office": map[string]interface{}{
				"app_id":     config.Env("WECHAT_APPID"),
				"app_secret": config.Env("WECHAT_APP_SECRET"),
				"token":      config.Env("WECHAT_TOKEN", "sweetsunnyflower"),
			},
			"jssdk": map[string]interface{}{
				"url":   config.Env("WECHAT_URL", ""),
				"debug": config.Env("WECHAT_DEBUG", true),
				"beta":  config.Env("WECHAT_BETA", false),
			},
		}
	})
}
