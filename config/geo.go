package config

import (
	"sweetsunnyflower/pkg/config"
)

func init() {
	config.Add("geo", func() map[string]interface{} {
		return map[string]interface{}{

			"api_url": config.Env("GEO_API_URL", ""),

			"api_key": config.Env("GEO_API_KEY", ""),
		}
	})
}
