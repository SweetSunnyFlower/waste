package config

import "sweetsunnyflower/pkg/config"

func init() {
	config.Add("cors", func() map[string]interface{} {
		return map[string]interface{}{
			"origin": config.Env("CORS_ORIGINS", "https://h5.gaobingbing.site"),
		}
	})
}
