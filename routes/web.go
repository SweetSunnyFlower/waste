package routes

import (
	"embed"
	"sweetsunnyflower/pkg/config"
	"sweetsunnyflower/pkg/response"

	"github.com/gin-gonic/gin"
)

func RegisterWebRoutes(route *gin.Engine, f embed.FS) {
	route.GET("/", func(ctx *gin.Context) {
		response.HTML(ctx, "index.html", gin.H{
			"title": config.Get("app.name"),
		})
	})

	route.GET("/photos", func(ctx *gin.Context) {
		response.HTML(ctx, "photos.html", gin.H{
			"title": config.Get("app.name"),
		})
	})
}
