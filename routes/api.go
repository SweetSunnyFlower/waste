package routes

import (
	"fmt"
	"sweetsunnyflower/app/http/controllers/api/v1/auth"

	"sweetsunnyflower/app/http/middlewares"
	"sweetsunnyflower/pkg/geo"
	"sweetsunnyflower/pkg/websocket"

	controllers "sweetsunnyflower/app/http/controllers/api/v1"

	"github.com/gin-gonic/gin"
)

func RegisterApiRoutes(route *gin.Engine) {

	WechatController := new(controllers.WechatsController)
	AddressesController := new(controllers.AddressesController)
	SignupController := new(auth.SignupController)
	VerifyCodeController := new(auth.VerifyCodeController)
	LoginController := new(auth.LoginController)
	UsersController := new(controllers.UsersController)
	UserCarsController := new(controllers.UserCarsController)
	UserPassengersController := new(controllers.UserPassengersController)
	UserFollowsController := new(controllers.UserFollowsController)
	UserBlacklistsController := new(controllers.UserBlacklistsController)
	UserTripsController := new(controllers.UserTripsController)
	CommonController := new(controllers.CommonController)
	TripPassengersController := new(controllers.TripPassengersController)
	NotifyController := new(controllers.NotifiesController)
	SignsController := new(controllers.SignsController)
	PointsController := new(controllers.PointsController)
	SuggestsController := new(controllers.SuggestsController)
	ChatsController := new(controllers.ChatsController)
	PhotosController := new(controllers.PhotosController)

	// 重置密码
	PasswordController := new(auth.PasswordController)

	wechat := route.Group("/wechat")
	{
		// 微信通知接口
		wechat.GET("/notify", WechatController.Notify)
		// 微信授权接口 code置换用户信息
		wechat.GET("/oauth2", WechatController.OAuth2)

		wechat.GET("jssdk-config", WechatController.GetJSSDKConfig)
	}

	v1 := route.Group("/v1")

	// middlewares.LimitIP("200-H")
	v1.Use()
	{
		v1.GET("/", func(ctx *gin.Context) {

			keyword := ctx.Query("keyword")

			geoClient := geo.NewGeo()
			res, err := geoClient.Search(keyword)
			if err != nil {
				ctx.JSON(200, gin.H{
					"error": err.Error(),
				})
				return
			}
			ctx.JSON(200, gin.H{
				"res": res,
			})
			return
			// 发送短信验证码
			// verifycode.NewVerifyCode("redis").SendSMS("18335908792")

			// from := ctx.Query("from")

			to := ctx.Query("to")

			// message := ctx.Query("message")

			// 通过用户ID查找连接
			manager := websocket.NewClientManager()
			// ws, ok := manager.ClientMap[from]
			// if !ok {
			// 	ctx.JSON(200, gin.H{
			// 		"message": "user not connected",
			// 	})
			// 	return
			// }

			toWs, ok := manager.ClientMap[to]
			fmt.Println(manager.ClientMap)
			if !ok {
				ctx.JSON(200, gin.H{
					"message": "user not connected",
				})
				return
			}

			// manager.Broadcast(message, websocket.TextMessage, nil)

			toWs.SendTo(toWs, "{\"action\":\"test\",\"payload\":\"234\"}", websocket.TextMessage, false)
			ctx.JSON(200, gin.H{
				"message": "Welcome to the API",
			})
		})

		AddressGroup := v1.Group("/addresses")
		{
			AddressGroup.GET("", AddressesController.Index)
		}

		authGroup := v1.Group("/auth")
		{
			authGroup.POST("/signup/phone/exist", middlewares.GuestJWT(), middlewares.LimitPerRoute("60-H"), SignupController.IsPhoneExist)
			authGroup.POST("/signup/email/exist", middlewares.GuestJWT(), middlewares.LimitPerRoute("60-H"), SignupController.IsEmailExist)

			// 注册接口
			authGroup.POST("/signup/using-phone", middlewares.GuestJWT(), SignupController.SignupUsingPhone)
			authGroup.POST("/signup/using-email", middlewares.GuestJWT(), SignupController.SignupUsingEmail)
			// 登录接口
			authGroup.POST("/login/using-phone", middlewares.GuestJWT(), LoginController.LoginByPhone)
			authGroup.POST("/login/using-password", middlewares.GuestJWT(), LoginController.LoginByPassword)

			// 刷新token
			authGroup.POST("/login/refresh-token", middlewares.AuthJWT(), LoginController.RefreshToken)
			// 重置密码
			authGroup.POST("/password-reset/using-phone", middlewares.GuestJWT(), PasswordController.ResetByPhone)
			authGroup.POST("/password-reset/using-email", middlewares.GuestJWT(), PasswordController.ResetByEmail)

			// 图片验证码接口
			authGroup.POST("/verify-codes/captcha", middlewares.LimitPerRoute("60-H"), VerifyCodeController.ShowCaptcha)
			// 短信验证码接口
			authGroup.POST("/verify-codes/phone", middlewares.LimitPerRoute("60-H"), VerifyCodeController.SendUsingPhone)
			// 邮箱验证码接口
			authGroup.POST("/verify-codes/email", middlewares.LimitPerRoute("60-H"), VerifyCodeController.SendUsingEmail)

		}

		commonGroup := v1.Group("/common")
		{
			// middlewares.AuthJWT(),
			commonGroup.POST("/upload/image", CommonController.UploadImage)
		}

		// 当前用户信息
		v1.GET("/user", middlewares.AuthJWT(), UsersController.CurrentUser)

		// 行程列表
		v1.GET("/trips", UserTripsController.Index)

		// 聊天记录
		chatsGroup := v1.Group("/chats", middlewares.AuthJWT())
		{
			chatsGroup.GET("", ChatsController.Index)
		}

		photosGroup := v1.Group("/photos")
		{
			photosGroup.GET("", PhotosController.Index)
			photosGroup.POST("", PhotosController.Store)
			photosGroup.PUT("/:id/:type", PhotosController.Update)
		}

		usersGroup := v1.Group("/users", middlewares.AuthJWT())
		{

			usersGroup.GET("", UsersController.Index)
			usersGroup.POST("/avatar", UsersController.UpdateAvatar)
			usersGroup.PUT("/phone", UsersController.UpdatePhone)
			usersGroup.PUT("/nickname", UsersController.UpdateNickname)
			usersGroup.PUT("/work", UsersController.UpdateWork)
			usersGroup.PUT("/gender", UsersController.UpdateGender)
			usersGroup.PUT("/age", UsersController.UpdateAge)
			// 实名认证
			usersGroup.POST("/certification", UsersController.Certification)

			// 最后一次认证信息
			usersGroup.GET("/certification", UsersController.LatestCertification)

			suggestsGroup := usersGroup.Group("/suggests")
			{
				suggestsGroup.POST("", SuggestsController.Store)
			}

			// 消息通知
			messagesGroup := usersGroup.Group("notifies")
			{
				messagesGroup.GET("", NotifyController.Index)
			}

			// 用户车辆
			usersCarsGroup := usersGroup.Group("/cars")
			{
				// 用户车辆列表
				usersCarsGroup.GET("", UserCarsController.Index)
				// 用户添加车辆
				usersCarsGroup.POST("", UserCarsController.Store)
				// 用户修改车辆
				usersCarsGroup.PUT("/:id", UserCarsController.Update)
				// 用户删除车辆
				usersCarsGroup.DELETE("/:id", UserCarsController.Delete)
			}

			// 用户乘车人
			usersPassengersGroup := usersGroup.Group("/passengers")
			{
				// 用户乘客列表
				usersPassengersGroup.GET("", UserPassengersController.Index)
				// 用户添加乘客
				usersPassengersGroup.POST("", UserPassengersController.Store)
				// 用户修改乘客
				usersPassengersGroup.PUT("/:id", UserPassengersController.Update)
				// 用户删除乘客
				usersPassengersGroup.DELETE("/:id", UserPassengersController.Delete)
			}

			// 用户签到
			usersSignsGroup := usersGroup.Group("/signs")
			{
				// 用户签到列表
				usersSignsGroup.GET("", SignsController.Index)

				// 用户签到
				usersSignsGroup.POST("", SignsController.Store)
			}

			// 用户积分
			usersPointsGroup := usersGroup.Group("/points")
			{
				// 用户积分列表
				usersPointsGroup.GET("", PointsController.Index)
			}

			// 用户关注
			usersFollowersGroup := usersGroup.Group("/follows")
			{
				// 用户关注列表
				usersFollowersGroup.GET("", UserFollowsController.Index)
				// 用户关注
				usersFollowersGroup.POST("", UserFollowsController.Store)
				// 用户取消关注
				usersFollowersGroup.DELETE("/:id", UserFollowsController.Delete)
			}

			// 用户拉黑
			usersBlacklistGroup := usersGroup.Group("/blacks")
			{
				// 用户拉黑列表
				usersBlacklistGroup.GET("", UserBlacklistsController.Index)
				// 用户拉黑
				usersBlacklistGroup.POST("", UserBlacklistsController.Store)
				// 用户取消拉黑
				usersBlacklistGroup.DELETE("/:id", UserBlacklistsController.Delete)
			}
			// 用户发布行程
			usersTripsGroup := usersGroup.Group("/trips")
			{
				// 用户行程列表
				usersTripsGroup.GET("", UserTripsController.Index)
				// 用户行程详情
				usersTripsGroup.GET("/:id", UserTripsController.Show)
				// 分享行程
				usersTripsGroup.GET("/share/:id", UserTripsController.Share)
				// 用户行程创建
				usersTripsGroup.POST("", UserTripsController.Store)
				// 用户行程修改
				usersTripsGroup.PUT("/:id", UserTripsController.Update)
				// 用户修改座位数
				usersTripsGroup.POST("/:id/seats", UserTripsController.ModifyRemaining)
				// 用户行程删除
				usersTripsGroup.DELETE("/:id", UserTripsController.Delete)
				// 发车
				usersTripsGroup.POST("/:id/start", UserTripsController.Start)
				// 取消行程
				usersTripsGroup.POST("/:id/cancel", UserTripsController.Cancel)
				// 确认行程
				usersTripsGroup.POST("/:id/confirm", TripPassengersController.Confirm)
				// 拒绝行程
				usersTripsGroup.POST("/:id/refuse", TripPassengersController.Refuse)

			}

			// 用户预定行程
			usersBookingsGroup := usersGroup.Group("/bookings")
			{
				// 用户预定行程
				usersBookingsGroup.POST("", TripPassengersController.Store)
				// 乘客取消拼车
				usersBookingsGroup.POST("/:id/cancel", TripPassengersController.Cancel)
				// 乘客预定的行程
				usersBookingsGroup.GET("", TripPassengersController.Index)
			}
		}
	}

}
