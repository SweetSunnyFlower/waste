package routes

import (
	"net/http"

	controllers "sweetsunnyflower/app/http/controllers/api/v1"
	"sweetsunnyflower/app/http/middlewares"
	"sweetsunnyflower/pkg/auth"
	websocketServer "sweetsunnyflower/pkg/websocket"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:    4096,
	WriteBufferSize:   4096,
	EnableCompression: true,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func RegisterWebsocketRoutes(route *gin.Engine) {

	// 需要登录才能连接wss
	route.GET("/ws", middlewares.WebsocketAuth(), func(c *gin.Context) {

		id := auth.CurrentUID(c)

		ws, err := upgrader.Upgrade(c.Writer, c.Request, nil)
		if err != nil {
			return
		}

		websocketServer.NewClientManager().Connect(id, ws)

		// 注册action
		websocketServer.NewClientManager().RegisterController(controllers.Distribute)
	})
}
