package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type UserPassengerRequest struct {
	Name  string `valid:"name" form:"name"`
	Phone string `valid:"phone" form:"phone"`
}

func UserPassengerSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"name":  []string{"required"},
		"phone": []string{"required"},
	}
	messages := govalidator.MapData{
		"name":  []string{"required:请输入姓名"},
		"phone": []string{"required:请输入手机号"},
	}
	return validate(data, rules, messages)
}
