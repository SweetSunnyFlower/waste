package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type UserTripsQueryRequest struct {
	Owner    bool   `valid:"owner" form:"owner,omitempty" json:"owner"`
	Status   uint8  `valid:"status" form:"status,omitempty" json:"status"`
	UserID   uint64 `valid:"user_id" form:"user_id,omitempty" json:"user_id"`
	StartID  uint64 `valid:"start_id" form:"start_id,omitempty" json:"start_id"`
	EndID    uint64 `valid:"end_id" form:"end_id,omitempty" json:"end_id"`
	Time     string `valid:"time" form:"time,omitempty" json:"time"`
	LatestId uint64 `valid:"latest_id" form:"latest_id,omitempty" json:"latest_id"`
	Limit    int    `valid:"limit" form:"limit,omitempty" json:"limit" default:"10"`
}

type UserTripRequest struct {
	StartID uint64 `valid:"start_id" form:"start_id" json:"start_id"`
	EndID   uint64 `valid:"end_id" form:"end_id" json:"end_id"`
	Time    string `valid:"time" form:"time" json:"time"`
	CarId   uint64 `valid:"car_id" form:"car_id" json:"car_id"`
	Sites   uint   `valid:"sites" form:"sites" json:"sites"`
	Price   uint   `valid:"price" form:"price" json:"price"`
	Remark  string `valid:"remark" form:"remark" json:"remark"`
}

type UserTripRemainingRequest struct {
	Type string `valid:"type" form:"type" json:"type"`
}

func UserTripRemaining(data interface{}, c *gin.Context) map[string][]string {
	rules := govalidator.MapData{
		"type": []string{"required"},
	}
	messages := govalidator.MapData{
		"type": []string{
			"required:类型为必填项",
		},
	}
	return validate(data, rules, messages)
}

func UserTripsQuery(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"start_id":  []string{"numeric"},
		"end_id":    []string{"numeric"},
		"time":      []string{"datetime"},
		"latest_id": []string{"numeric"},
		"user_id":   []string{"numeric"},
		"limit":     []string{"required", "numeric"},
	}

	messages := govalidator.MapData{
		"start_id":  []string{"numeric:起点ID必须是数字"},
		"end_id":    []string{"numeric:终点ID必须是数字"},
		"time":      []string{"datetime:时间格式不正确"},
		"latest_id": []string{"numeric:最新记录ID必须是数字"},
		"user_id":   []string{"numeric:用户ID必须是数字"},
		"limit":     []string{"numeric:查询条数必须是数字"},
	}

	return validate(data, rules, messages)
}

func UserTripSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"start_id": []string{"required", "numeric"},
		"end_id":   []string{"required", "numeric"},
		"time":     []string{"required", "datetime"},
		"car_id":   []string{"required", "numeric"},
		"sites":    []string{"required", "numeric", "min:1", "max:10"},
		"price":    []string{"required", "numeric", "between:1,100000"},
		"remark":   []string{"max_cn:255"},
	}
	messages := govalidator.MapData{
		"start_id": []string{
			"required:起点为必填项",
			"numeric:起点必须为数字",
		},
		"end_id": []string{
			"required:终点为必填项",
			"numeric:终点必须为数字",
		},
		"time": []string{
			"required:时间为必填项",
			"datetime:时间格式不正确",
		},
		"car_id": []string{
			"required:车型为必填项",
			"numeric:车型必须为数字",
		},
		"sites": []string{
			"required:座位数为必填项",
			"numeric:座位数必须为数字",
			"min:座位数必须大于1",
			"max:座位数最多10个",
		},
		"price": []string{
			"required:价格为必填项",
			"numeric:价格必须为数字",
			"between:价格必须大于1且小于10万",
		},
		"remark": []string{
			"max_cn:备注最多255个字符",
		},
	}
	return validate(data, rules, messages)
}
