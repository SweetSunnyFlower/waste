package requests

import (
	"mime/multipart"

	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type UserUpdateAvatarRequest struct {
	Avatar *multipart.FileHeader `valid:"avatar" form:"avatar"`
}

type UpdatePhoneRequest struct {
	Phone string `valid:"phone" form:"phone"`
}

type UpdateNicknameRequest struct {
	Nickname string `valid:"nickname" form:"nickname"`
}

type UpdateWorkRequest struct {
	Work string `valid:"work" form:"work"`
}

type UpdateGenderRequest struct {
	Gender uint8 `valid:"gender" form:"gender"`
}

type UpdateAgeRequest struct {
	Age string `valid:"age" form:"age"`
}

type CertificationRequest struct {
	Name        string `valid:"name" form:"name" json:"name"`
	IdCard      string `valid:"id_card" form:"id_card" json:"id_card"`
	IdCardFront string `valid:"id_card_front" form:"id_card_front" json:"id_card_front"`
	IdCardBack  string `valid:"id_card_back" form:"id_card_back" json:"id_card_back"`
}

func Certification(data interface{}, c *gin.Context) map[string][]string {
	rules := govalidator.MapData{
		"name":          []string{"required"},
		"id_card":       []string{"required"},
		"id_card_front": []string{"required"},
		"id_card_back":  []string{"required"},
	}
	messages := govalidator.MapData{
		"name": []string{
			"required:请输入真实姓名",
		},
		"id_card": []string{
			"required:请输入身份证号",
		},
		"id_card_front": []string{
			"required:请上传身份证正面",
		},
		"id_card_back": []string{
			"required:请上传身份证反面",
		},
	}

	return validate(data, rules, messages)
}

func UpdateAge(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"age": []string{"required"},
	}
	messages := govalidator.MapData{
		"age": []string{
			"required:请输入选择年龄",
		},
	}

	return validate(data, rules, messages)
}

func UpdateGender(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"gender": []string{"required"},
	}
	messages := govalidator.MapData{
		"gender": []string{
			"required:请输入选择性别",
		},
	}

	return validate(data, rules, messages)
}

func UpdateWork(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"work": []string{"required"},
	}
	messages := govalidator.MapData{
		"work": []string{
			"required:请输入选择单位",
		},
	}

	return validate(data, rules, messages)
}

func UpdateNickname(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"nickname": []string{"required"},
	}
	messages := govalidator.MapData{
		"nickname": []string{
			"required:请输入昵称",
		},
	}

	return validate(data, rules, messages)
}

func UpdatePhone(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"phone": []string{"required"},
	}
	messages := govalidator.MapData{
		"phone": []string{
			"required:请输入手机号",
		},
	}

	return validate(data, rules, messages)
}

func UserUpdateAvatar(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		// size 的单位为 bytes
		// - 1024 bytes 为 1kb
		// - 1048576 bytes 为 1mb
		// - 20971520 bytes 为 20mb
		"file:avatar": []string{"ext:png,jpg,jpeg", "size:20971520", "required"},
	}
	messages := govalidator.MapData{
		"file:avatar": []string{
			"ext:ext头像只能上传 png, jpg, jpeg 任意一种的图片",
			"size:头像文件最大不能超过 20MB",
			"required:必须上传图片",
		},
	}

	return validateFile(c, data, rules, messages)
}
