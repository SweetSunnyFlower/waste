package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type UserFollowRequest struct {
	FollowID uint64 `valid:"follow_id" form:"follow_id" json:"follow_id"`
}

func UserFollowSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"follow_id": []string{"required"},
	}
	messages := govalidator.MapData{
		"follow_id": []string{
			"required:关注用户ID为必填项",
		},
	}
	return validate(data, rules, messages)
}
