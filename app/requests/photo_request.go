package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type PhotoRequest struct {
	Name      string `valid:"name" form:"name" json:"name"`
	OriginURL string `valid:"origin_url" form:"origin_url" json:"origin_url"`
}

type PhotoPSRequest struct {
	PsURL string `valid:"ps_url" form:"ps_url" json:"ps_url"`
}

type PhotoRemarkRequest struct {
	Remark string `valid:"remark" form:"remark" json:"remark"`
}

type PhotoDemoRequest struct {
	DemoURL string `valid:"demo_url" form:"demo_url" json:"demo_url"`
}

func PhotoSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"name":       []string{"required"},
		"origin_url": []string{"required"},
	}
	messages := govalidator.MapData{
		"name": []string{
			"required:图片名称为必填项",
		},
		"origin_url": []string{
			"required:图片地址为必填项",
		},
	}
	return validate(data, rules, messages)
}

func PhotoPSSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"ps_url": []string{"required"},
	}
	messages := govalidator.MapData{
		"ps_url": []string{
			"required:图片地址为必填项",
		},
	}
	return validate(data, rules, messages)
}
func PhotoDemoSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"demo_url": []string{"required"},
	}
	messages := govalidator.MapData{
		"demo_url": []string{
			"required:图片地址为必填项",
		},
	}
	return validate(data, rules, messages)
}

func PhotoRemarkSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"remark": []string{"required"},
	}
	messages := govalidator.MapData{
		"remark": []string{
			"required:备注为必填项",
		},
	}
	return validate(data, rules, messages)
}
