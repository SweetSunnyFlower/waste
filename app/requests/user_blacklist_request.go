package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type UserBlacklistRequest struct {
	BlackID uint64 `valid:"black_id" form:"black_id"`
}

func UserBlacklistSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"black_id": []string{"required"},
	}
	messages := govalidator.MapData{
		"black_id": []string{
			"required:黑名单ID为必填项",
		},
	}
	return validate(data, rules, messages)
}
