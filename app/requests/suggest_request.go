package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type SuggestRequest struct {
	Type    string `valid:"type" form:"name"`
	Suggest string `valid:"suggest" form:"suggest"`
	Contact string `valid:"contact" form:"contact,omitempty"`
}

func SuggestSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"type":    []string{"required"},
		"suggest": []string{"required"},
	}
	messages := govalidator.MapData{
		"type": []string{
			"required:请选择反馈类型",
		},
		"suggest": []string{
			"required:请填写反馈内容",
		},
	}
	return validate(data, rules, messages)
}
