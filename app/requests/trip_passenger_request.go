package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type TripPassengersQueryRequest struct {
	UserID   uint64 `valid:"user_id" form:"user_id,omitempty" json:"user_id"`
	Status   uint64 `valid:"status" form:"status" json:"status"`
	Time     string `valid:"time" form:"time,omitempty" json:"time"`
	LatestId uint64 `valid:"latest_id" form:"latest_id,omitempty" json:"latest_id"`
	Limit    int    `valid:"limit" form:"limit,omitempty" json:"limit" default:"10"`
}

type TripPassengerRequest struct {
	TripID      uint64 `valid:"trip_id" form:"trip_id" json:"trip_id"`
	PassengerID uint64 `valid:"passenger_id" form:"passenger_id" json:"passenger_id"`
	Booking     uint   `valid:"booking" form:"booking" json:"booking"` // 预定座位数
}

func TripPassengersQuery(data interface{}, c *gin.Context) map[string][]string {
	rules := govalidator.MapData{
		"status":    []string{"numeric"},
		"time":      []string{"datetime"},
		"latest_id": []string{"numeric"},
		"limit":     []string{"numeric"},
	}

	messages := govalidator.MapData{
		"status": []string{
			"required:请选择状态",
			"numeric:状态必须为数字",
		},
		"time": []string{
			"required:请选择时间",
			"date_format:时间格式不正确",
		},
		"latest_id": []string{
			"numeric:最新ID必须为数字",
			"required:请选择时间",
		},
		"limit": []string{
			"numeric:每页数量必须为数字",
		},
	}

	return validate(data, rules, messages)

}

func TripPassengerSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"trip_id":      []string{"required", "numeric"},
		"passenger_id": []string{"required", "numeric"},
		"booking":      []string{"required", "numeric"},
	}
	messages := govalidator.MapData{
		"trip_id": []string{
			"required:行程ID为必填项",
			"numeric:行程ID必须为数字",
		},
		"passenger_id": []string{
			"required:请选择乘客",
			"numeric:乘客ID必须为数字",
		},
		"booking": []string{
			"required:请选择座位",
			"numeric:预定座位数必须为数字",
		},
	}
	return validate(data, rules, messages)
}
