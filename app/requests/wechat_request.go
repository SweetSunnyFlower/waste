package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type WechatRequest struct {
	Code string `valid:"code" form:"code"`
}

func WechatCode(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"code": []string{"required"},
	}
	messages := govalidator.MapData{
		"code": []string{
			"required:code为必填项",
		},
	}
	return validate(data, rules, messages)
}
