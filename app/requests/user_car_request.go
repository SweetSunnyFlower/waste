package requests

import (
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
)

type UserCarRequest struct {
	Brand    string `valid:"brand" form:"brand"`
	Color    string `valid:"color" form:"color"`
	Plate    string `valid:"plate" form:"plate"`
	Sites    uint   `valid:"sites" form:"sites"`
	UseYears uint   `valid:"use_years" form:"use_years"`
}

func UserCarSave(data interface{}, c *gin.Context) map[string][]string {

	rules := govalidator.MapData{
		"brand":     []string{"required"},
		"color":     []string{"required"},
		"plate":     []string{"required"},
		"sites":     []string{"required"},
		"use_years": []string{"required"},
	}
	messages := govalidator.MapData{
		"brand": []string{
			"required:名称为必填项",
			"min_cn:名称长度需至少 2 个字",
			"max_cn:名称长度不能超过 20 个字",
		},
		"color": []string{
			"required:颜色为必填项",
			"min_cn:颜色长度需至少 1 个字",
			"max_cn:颜色长度不能超过8个字",
		},
		"plate": []string{
			"required:车牌为必填项",
			"min_cn:车牌长度需至少 1 个字",
			"max_cn:车牌长度不能超过 20 个字",
		},
		"sites": []string{
			"required:座位数为必填项",
			"min:座位数最少为1",
		},
		"use_years": []string{
			"required:使用年限为必填项",
			"min:使用年限长度做少为1",
		},
	}
	return validate(data, rules, messages)
}
