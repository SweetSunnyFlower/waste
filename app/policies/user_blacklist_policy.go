package policies

import (
	"sweetsunnyflower/app/models/user_blacklist"
	"sweetsunnyflower/pkg/auth"

	"github.com/gin-gonic/gin"
)

func CanModifyUserBlacklist(c *gin.Context, userBlacklistModel user_blacklist.UserBlacklist) bool {
	return auth.CurrentIntUID(c) == userBlacklistModel.UserID
}

// func CanViewUserBlacklist(c *gin.Context, userBlacklistModel user_blacklist.UserBlacklist) bool {}
// func CanCreateUserBlacklist(c *gin.Context, userBlacklistModel user_blacklist.UserBlacklist) bool {}
// func CanUpdateUserBlacklist(c *gin.Context, userBlacklistModel user_blacklist.UserBlacklist) bool {}
// func CanDeleteUserBlacklist(c *gin.Context, userBlacklistModel user_blacklist.UserBlacklist) bool {}
