package policies

import (
	"sweetsunnyflower/app/models/user_passenger"
	"sweetsunnyflower/pkg/auth"

	"github.com/gin-gonic/gin"
)

func CanModifyUserPassenger(c *gin.Context, userPassengerModel user_passenger.UserPassenger) bool {
	return auth.CurrentIntUID(c) == userPassengerModel.UserID
}

// func CanViewUserPassenger(c *gin.Context, userPassengerModel user_passenger.UserPassenger) bool {}
// func CanCreateUserPassenger(c *gin.Context, userPassengerModel user_passenger.UserPassenger) bool {}
// func CanUpdateUserPassenger(c *gin.Context, userPassengerModel user_passenger.UserPassenger) bool {}
// func CanDeleteUserPassenger(c *gin.Context, userPassengerModel user_passenger.UserPassenger) bool {}
