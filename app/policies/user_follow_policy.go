package policies

import (
	"sweetsunnyflower/app/models/user_follow"
	"sweetsunnyflower/pkg/auth"

	"github.com/gin-gonic/gin"
)

func CanModifyUserFollow(c *gin.Context, userFollowModel user_follow.UserFollow) bool {
	return auth.CurrentIntUID(c) == userFollowModel.UserID
}

// func CanViewUserFollow(c *gin.Context, userFollowModel user_follow.UserFollow) bool {}
// func CanCreateUserFollow(c *gin.Context, userFollowModel user_follow.UserFollow) bool {}
// func CanUpdateUserFollow(c *gin.Context, userFollowModel user_follow.UserFollow) bool {}
// func CanDeleteUserFollow(c *gin.Context, userFollowModel user_follow.UserFollow) bool {}
