package policies

import (
	"sweetsunnyflower/app/models/notify"
	"sweetsunnyflower/pkg/auth"

	"github.com/gin-gonic/gin"
)

func CanModifyNotify(c *gin.Context, notifyModel notify.Notify) bool {
	return auth.CurrentIntUID(c) == notifyModel.UserID
}

// func CanViewNotify(c *gin.Context, notifyModel notify.Notify) bool {}
// func CanCreateNotify(c *gin.Context, notifyModel notify.Notify) bool {}
// func CanUpdateNotify(c *gin.Context, notifyModel notify.Notify) bool {}
// func CanDeleteNotify(c *gin.Context, notifyModel notify.Notify) bool {}
