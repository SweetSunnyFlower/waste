package policies

import (
    "sweetsunnyflower/app/models/user_trip"
    "sweetsunnyflower/pkg/auth"

    "github.com/gin-gonic/gin"
)

func CanModifyUserTrip(c *gin.Context, userTripModel user_trip.UserTrip) bool {
    return auth.CurrentIntUID(c) == userTripModel.UserID
}

// func CanViewUserTrip(c *gin.Context, userTripModel user_trip.UserTrip) bool {}
// func CanCreateUserTrip(c *gin.Context, userTripModel user_trip.UserTrip) bool {}
// func CanUpdateUserTrip(c *gin.Context, userTripModel user_trip.UserTrip) bool {}
// func CanDeleteUserTrip(c *gin.Context, userTripModel user_trip.UserTrip) bool {}
