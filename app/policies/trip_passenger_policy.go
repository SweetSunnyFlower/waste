package policies

import (
    "sweetsunnyflower/app/models/trip_passenger"
    "sweetsunnyflower/pkg/auth"

    "github.com/gin-gonic/gin"
)

func CanModifyTripPassenger(c *gin.Context, tripPassengerModel trip_passenger.TripPassenger) bool {
    return auth.CurrentIntUID(c) == tripPassengerModel.UserID
}

// func CanViewTripPassenger(c *gin.Context, tripPassengerModel trip_passenger.TripPassenger) bool {}
// func CanCreateTripPassenger(c *gin.Context, tripPassengerModel trip_passenger.TripPassenger) bool {}
// func CanUpdateTripPassenger(c *gin.Context, tripPassengerModel trip_passenger.TripPassenger) bool {}
// func CanDeleteTripPassenger(c *gin.Context, tripPassengerModel trip_passenger.TripPassenger) bool {}
