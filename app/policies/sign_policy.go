package policies

import (
    "sweetsunnyflower/app/models/sign"
    "sweetsunnyflower/pkg/auth"

    "github.com/gin-gonic/gin"
)

func CanModifySign(c *gin.Context, signModel sign.Sign) bool {
    return auth.CurrentIntUID(c) == signModel.UserID
}

// func CanViewSign(c *gin.Context, signModel sign.Sign) bool {}
// func CanCreateSign(c *gin.Context, signModel sign.Sign) bool {}
// func CanUpdateSign(c *gin.Context, signModel sign.Sign) bool {}
// func CanDeleteSign(c *gin.Context, signModel sign.Sign) bool {}
