package policies

import (
	"sweetsunnyflower/app/models/photo"

	"github.com/gin-gonic/gin"
)

func CanModifyPhoto(c *gin.Context, photoModel photo.Photo) bool {
	return true
}

// func CanViewPhoto(c *gin.Context, photoModel photo.Photo) bool {}
// func CanCreatePhoto(c *gin.Context, photoModel photo.Photo) bool {}
// func CanUpdatePhoto(c *gin.Context, photoModel photo.Photo) bool {}
// func CanDeletePhoto(c *gin.Context, photoModel photo.Photo) bool {}
