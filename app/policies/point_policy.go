package policies

import (
    "sweetsunnyflower/app/models/point"
    "sweetsunnyflower/pkg/auth"

    "github.com/gin-gonic/gin"
)

func CanModifyPoint(c *gin.Context, pointModel point.Point) bool {
    return auth.CurrentIntUID(c) == pointModel.UserID
}

// func CanViewPoint(c *gin.Context, pointModel point.Point) bool {}
// func CanCreatePoint(c *gin.Context, pointModel point.Point) bool {}
// func CanUpdatePoint(c *gin.Context, pointModel point.Point) bool {}
// func CanDeletePoint(c *gin.Context, pointModel point.Point) bool {}
