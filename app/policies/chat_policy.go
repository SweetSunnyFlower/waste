package policies

import (
    "sweetsunnyflower/app/models/chat"
    "sweetsunnyflower/pkg/auth"

    "github.com/gin-gonic/gin"
)

func CanModifyChat(c *gin.Context, chatModel chat.Chat) bool {
    return auth.CurrentIntUID(c) == chatModel.UserID
}

// func CanViewChat(c *gin.Context, chatModel chat.Chat) bool {}
// func CanCreateChat(c *gin.Context, chatModel chat.Chat) bool {}
// func CanUpdateChat(c *gin.Context, chatModel chat.Chat) bool {}
// func CanDeleteChat(c *gin.Context, chatModel chat.Chat) bool {}
