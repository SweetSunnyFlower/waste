package policies

import (
	"sweetsunnyflower/app/models/user_car"
	"sweetsunnyflower/pkg/auth"

	"github.com/gin-gonic/gin"
)

func CanModifyUserCar(c *gin.Context, userCarModel user_car.UserCar) bool {
	return auth.CurrentIntUID(c) == userCarModel.UserID
}

// func CanViewUserCar(c *gin.Context, userCarModel user_car.UserCar) bool {}
// func CanCreateUserCar(c *gin.Context, userCarModel user_car.UserCar) bool {}
// func CanUpdateUserCar(c *gin.Context, userCarModel user_car.UserCar) bool {}
// func CanDeleteUserCar(c *gin.Context, userCarModel user_car.UserCar) bool {}
