package policies

import (
    "sweetsunnyflower/app/models/suggest"
    "sweetsunnyflower/pkg/auth"

    "github.com/gin-gonic/gin"
)

func CanModifySuggest(c *gin.Context, suggestModel suggest.Suggest) bool {
    return auth.CurrentIntUID(c) == suggestModel.UserID
}

// func CanViewSuggest(c *gin.Context, suggestModel suggest.Suggest) bool {}
// func CanCreateSuggest(c *gin.Context, suggestModel suggest.Suggest) bool {}
// func CanUpdateSuggest(c *gin.Context, suggestModel suggest.Suggest) bool {}
// func CanDeleteSuggest(c *gin.Context, suggestModel suggest.Suggest) bool {}
