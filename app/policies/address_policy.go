package policies

import (
	"sweetsunnyflower/app/models/address"

	"github.com/gin-gonic/gin"
)

func CanModifyAddress(c *gin.Context, addressModel address.Address) bool {
	return true
}

// func CanViewAddress(c *gin.Context, addressModel address.Address) bool {}
// func CanCreateAddress(c *gin.Context, addressModel address.Address) bool {}
// func CanUpdateAddress(c *gin.Context, addressModel address.Address) bool {}
// func CanDeleteAddress(c *gin.Context, addressModel address.Address) bool {}
