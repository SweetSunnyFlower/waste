package cmd

import (
	"sweetsunnyflower/app/models/user"
	"sweetsunnyflower/pkg/console"
	"sweetsunnyflower/pkg/jwt"

	"github.com/spf13/cobra"
)

var CmdUsertoken = &cobra.Command{
	Use:   "usertoken",
	Short: "HERE PUTS THE COMMAND DESCRIPTION",
	Run:   runUsertoken,
	Args:  cobra.ExactArgs(1), // 只允许且必须传 1 个参数
}

func runUsertoken(cmd *cobra.Command, args []string) {

	userID := args[0]

	// 查询用户是否存在
	userModel := user.GetById(userID)

	if userModel.ID > 0 {
		// token
		token := jwt.NewJWT().IssueToken(userModel.GetStringID(), userModel.Name)
		console.Success("token: " + token)
	} else {
		console.Error("用户ID不存在")
	}
}
