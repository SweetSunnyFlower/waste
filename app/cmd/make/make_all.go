package make

import (
	"github.com/spf13/cobra"
)

var CmdMakeAll = &cobra.Command{
	Use:   "all",
	Short: "Create all file, example make all user",
	Run:   runMakeAll,
	Args:  cobra.ExactArgs(1), // 只允许且必须传 1 个参数
}

func runMakeAll(cmd *cobra.Command, args []string) {

	MakeMigration(args)

	MakeModel(args)

	MakeFactory(args)

	MakeSeeder(args)

	MakeRequest(args)

	MakePolicy(args)

	MakeAPIController([]string{"v1" + "/" + args[0]})
}
