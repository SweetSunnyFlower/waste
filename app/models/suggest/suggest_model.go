// Package suggest 模型
package suggest

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

type Suggest struct {
	models.Model

	UserID  uint64 `gorm:"column:user_id;not null"`
	Type    string `gorm:"column:type;not null"`
	Suggest string `gorm:"column:suggest;not null"`
	Status  int    `gorm:"column:status;not null;default:0"`
	Contact string `gorm:"column:contact;not null;default:''"`

	models.CommonTimestampsField
}

func (suggest *Suggest) Create() {
	database.DB.Create(&suggest)
}

func (suggest *Suggest) Save() (rowsAffected int64) {
	result := database.DB.Save(&suggest)
	return result.RowsAffected
}

func (suggest *Suggest) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&suggest)
	return result.RowsAffected
}
