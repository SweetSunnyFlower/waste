package suggest

import (
    "sweetsunnyflower/pkg/database"
    "sweetsunnyflower/pkg/app"
    "sweetsunnyflower/pkg/paginator"

    "github.com/gin-gonic/gin"
)

func Get(idstr string) (suggest Suggest) {
    database.DB.Where("id", idstr).First(&suggest)
    return
}

func GetBy(field, value string) (suggest Suggest) {
    database.DB.Where("? = ?", field, value).First(&suggest)
    return
}

func All() (suggests []Suggest) {
    database.DB.Find(&suggests)
    return 
}

func IsExist(field, value string) bool {
    var count int64
    database.DB.Model(Suggest{}).Where("? = ?", field, value).Count(&count)
    return count > 0
}

func Paginate(c *gin.Context, perPage int) (suggests []Suggest, paging paginator.Paging) {
    paging = paginator.Paginate(
        c,
        database.DB.Model(Suggest{}),
        &suggests,
        app.V1URL(database.TableName(&Suggest{})),
        perPage,
    )
    return
}