package user_certification

import (
	"sweetsunnyflower/pkg/app"
	"sweetsunnyflower/pkg/database"
	"sweetsunnyflower/pkg/paginator"

	"github.com/gin-gonic/gin"
)

func Get(idstr string) (userCertification UserCertification) {
	database.DB.Where("id", idstr).First(&userCertification)
	return
}

func GetUserLatestCertification(userId string) (userCertification *UserCertification) {
	database.DB.Where("user_id = ?", userId).Order("id desc").First(&userCertification)
	return
}

func GetBy(field, value string) (userCertification UserCertification) {
	database.DB.Where("? = ?", field, value).First(&userCertification)
	return
}

func All() (userCertifications []UserCertification) {
	database.DB.Find(&userCertifications)
	return
}

func IsExist(field, value string) bool {
	var count int64
	database.DB.Model(UserCertification{}).Where("? = ?", field, value).Count(&count)
	return count > 0
}

func Paginate(c *gin.Context, perPage int) (userCertifications []UserCertification, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(UserCertification{}),
		&userCertifications,
		app.V1URL(database.TableName(&UserCertification{})),
		perPage,
	)
	return
}
