// Package user_certification 模型
package user_certification

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

const PENDING = 1 //待审核
const PASS = 2    //审核通过
const REJECT = 3  //审核拒绝

type UserCertification struct {
	models.Model
	UserID      uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	Name        string `gorm:"column:name;comment:姓名;index;" json:"name"`
	IdCard      string `gorm:"column:id_card;comment:身份证号;index;" json:"id_card"`
	IdCardFront string `gorm:"column:id_card_front;comment:身份证正面照;index;" json:"id_card_front"`
	IdCardBack  string `gorm:"column:id_card_back;comment:身份证反面照;index;" json:"id_card_back"`
	Status      uint8  `gorm:"column:status;comment:状态;comment:状态;index;" json:"status"`

	models.CommonTimestampsField
}

func (userCertification *UserCertification) Create() {
	database.DB.Create(&userCertification)
}

func (userCertification *UserCertification) Save() (rowsAffected int64) {
	result := database.DB.Save(&userCertification)
	return result.RowsAffected
}

func (userCertification *UserCertification) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&userCertification)
	return result.RowsAffected
}
