// Package user_follow 模型
package user_follow

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

type UserFollow struct {
	models.Model

	UserID   uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	FollowID uint64 `gorm:"column:follow_id;comment:关注用户ID;index;" json:"follow_id"`
	User     *User  `gorm:"foreignkey:follow_id;" json:"user"`

	models.CommonTimestampsField
}

type User struct {
	models.Model
	Name   string `gorm:"column:name;comment:用户名;" json:"name"`
	Avatar string `gorm:"column:avatar;comment:头像;" json:"avatar"`
}

func (userFollow *UserFollow) Create() {
	database.DB.Create(&userFollow)
}

func (userFollow *UserFollow) Save() (rowsAffected int64) {
	result := database.DB.Save(&userFollow)
	return result.RowsAffected
}

func (userFollow *UserFollow) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&userFollow)
	return result.RowsAffected
}
