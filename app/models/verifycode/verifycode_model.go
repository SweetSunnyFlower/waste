package verifycode

import (
	"sweetsunnyflower/app/models"
	"time"
)

type VerifyCode struct {
	models.Model

	Key        string        `gorm:"type:varchar(255);comment:key;not null;index" db:"key" json:"key"`
	Code       string        `gorm:"type:varchar(255);comment:验证码;not null;index" db:"code" json:"code"`
	Phone      string        `gorm:"type:varchar(20);comment:手机号;index;default:null"`
	Verify     uint8         `gorm:"comment:是否验证 0:否1:是;default:0;" db:"verify" json:"verify" form:"verify"`
	ExpireTime time.Duration `gorm:"comment:过期时间" json:"expire_time" db:"expire_time"`

	models.CommonTimestampsField
}
