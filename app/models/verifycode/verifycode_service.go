package verifycode

import (
	"sweetsunnyflower/pkg/database"
	"sweetsunnyflower/pkg/logger"
	"time"
)

func (vm *VerifyCode) Get(key string) string {

	verifyCode := &VerifyCode{}

	database.DB.Model(&VerifyCode{}).Where("key = ?", key).First(verifyCode)

	return verifyCode.Code
}

func (vm *VerifyCode) Set(key string, value string, expire time.Duration) bool {

	result := database.DB.Create(&VerifyCode{
		Key:        key,
		Code:       value,
		Phone:      "",
		ExpireTime: expire,
	})

	if result.Error != nil {
		logger.ErrorString("mysql verifycode set error:", "verify create error", result.Error.Error())
		return false
	}
	return true
}

func (vm *VerifyCode) Del(key string) bool {
	result := database.DB.Where("key = ?", key).Delete(&VerifyCode{})

	if result.Error != nil {
		logger.ErrorString("mysql verifycode del error:", "verify del error", result.Error.Error())
		return false
	}
	return true
}
