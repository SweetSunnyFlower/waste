// Package sign 模型
package sign

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

type Sign struct {
	models.Model

	UserID uint64 `gorm:"column:user_id;not null" json:"user_id"`
	Week   int    `gorm:"column:week;not null" json:"week"`

	models.CommonTimestampsField
}

func (sign *Sign) Create() {
	database.DB.Create(&sign)
}

func (sign *Sign) Save() (rowsAffected int64) {
	result := database.DB.Save(&sign)
	return result.RowsAffected
}

func (sign *Sign) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&sign)
	return result.RowsAffected
}
