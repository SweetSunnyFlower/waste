// Package user_trip 模型
package user_trip

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/app/models/trip_passenger"

	"sweetsunnyflower/app/http/dto/address"
	"sweetsunnyflower/app/http/dto/car"
	"sweetsunnyflower/app/http/dto/user"
	"sweetsunnyflower/pkg/database"
)

const PENDING = 1   // 等待发车，创建后的初始状态
const SUCCESS = 2   // 发车成功，这个状态是自动满员，或者司机自己确认发车
const CANCELLED = 3 // 取消行程，这个状态是司机自己主动取消的

type UserTrip struct {
	models.Model

	UserID    uint64           `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	StartID   uint64           `gorm:"column:start_id;comment:起点ID;index;" json:"start_id"`
	EndID     uint64           `gorm:"column:end_id;comment:终点ID;index;" json:"end_id"`
	Time      string           `gorm:"column:time;comment:起点时间;index;" json:"time"`
	CarId     uint64           `gorm:"column:car_id;comment:车辆ID;index;" json:"car_id"`
	Sites     uint             `gorm:"column:sites;comment:座位数量;index;" json:"sites"`
	Remaining uint             `gorm:"column:remaining;comment:剩余座位;index;" json:"remaining"`
	Price     uint             `gorm:"column:price;comment:价格;index;" json:"price"`
	Status    uint             `gorm:"column:status;comment:状态;index;" json:"status"`
	Remark    string           `gorm:"column:remark;comment:备注;" json:"remark"`
	Line      string           `gorm:"column:line;comment:线路;" json:"line"`
	Start     *address.Address `gorm:"foreignKey:start_id;" json:"start"`
	End       *address.Address `gorm:"foreignKey:end_id;" json:"end"`
	Car       *car.UserCar     `gorm:"foreignKey:car_id;" json:"car"`
	User      *user.User       `gorm:"foreignKey:user_id;" json:"user"`

	TripPassenger []*trip_passenger.TripPassenger `gorm:"foreignKey:trip_id;" json:"passengers,omitempty"`

	models.CommonTimestampsField
}

func (userTrip *UserTrip) Create() {
	database.DB.Create(&userTrip)
}

func (userTrip *UserTrip) Save() (rowsAffected int64) {
	result := database.DB.Save(&userTrip)
	return result.RowsAffected
}

func (userTrip *UserTrip) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&userTrip)
	return result.RowsAffected
}
