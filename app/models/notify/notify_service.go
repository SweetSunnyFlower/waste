package notify

import (
	"sweetsunnyflower/app/requests"
	"sweetsunnyflower/pkg/app"
	"sweetsunnyflower/pkg/database"
	"sweetsunnyflower/pkg/paginator"

	"github.com/gin-gonic/gin"
)

func Get(idstr string) (notify Notify) {
	database.DB.Where("id", idstr).First(&notify)
	return
}

func GetBy(field, value string) (notify Notify) {
	database.DB.Where("? = ?", field, value).First(&notify)
	return
}

func All() (notifies []Notify) {
	database.DB.Find(&notifies)
	return
}

func IsExist(field, value string) bool {
	var count int64
	database.DB.Model(Notify{}).Where("? = ?", field, value).Count(&count)
	return count > 0
}

func Paginate(c *gin.Context, perPage int) (notifies []Notify, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(Notify{}),
		&notifies,
		app.V1URL(database.TableName(&Notify{})),
		perPage,
	)
	return
}

func GetByLatestId(query requests.NotifyQueryRequest) (notifies []Notify) {

	model := database.DB

	if query.UserID > 0 {
		model = model.Where("user_id = ?", query.UserID)
	}

	if query.Status > 0 {
		model = model.Where("status = ?", query.Status)
	}

	if query.LatestId > 0 {
		model = model.Where("id < ?", query.LatestId)
	}

	model.Order("id desc").Limit(query.Limit).Find(&notifies)

	return
}
