// Package notify 模型
package notify

import (
	"encoding/json"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

const (
	// 实名认证提醒通知
	Certification = 1
)

type Content struct {
	Icon    string `json:"icon"`
	Title   string `json:"title"`
	Message string `json:"message"`
	Target  string `json:"target"`
}

func (c *Content) Marshal() ([]byte, error) {
	return json.Marshal(c)
}

func UnmarshalContent(data []byte) (Content, error) {
	var r Content
	err := json.Unmarshal(data, &r)
	return r, err
}

type Notify struct {
	models.Model

	UserID  uint64 `gorm:"column:user_id;comment:用户id;index;" db:"user_id;index;" json:"user_id" form:"user_id"`
	Type    uint64 `gorm:"column:type;" db:"type" json:"type"`
	Content string `gorm:"column:content;" db:"content" json:"content"`
	Status  uint8  `gorm:"column:status;index;" db:"status" json:"status"`
	models.CommonTimestampsField
}

func (notify *Notify) Create() {
	database.DB.Create(&notify)
}

func (notify *Notify) Save() (rowsAffected int64) {
	result := database.DB.Save(&notify)
	return result.RowsAffected
}

func (notify *Notify) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&notify)
	return result.RowsAffected
}
