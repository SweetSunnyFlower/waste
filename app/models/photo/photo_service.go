package photo

import (
    "sweetsunnyflower/pkg/database"
    "sweetsunnyflower/pkg/app"
    "sweetsunnyflower/pkg/paginator"

    "github.com/gin-gonic/gin"
)

func Get(idstr string) (photo Photo) {
    database.DB.Where("id", idstr).First(&photo)
    return
}

func GetBy(field, value string) (photo Photo) {
    database.DB.Where("? = ?", field, value).First(&photo)
    return
}

func All() (photos []Photo) {
    database.DB.Find(&photos)
    return 
}

func IsExist(field, value string) bool {
    var count int64
    database.DB.Model(Photo{}).Where("? = ?", field, value).Count(&count)
    return count > 0
}

func Paginate(c *gin.Context, perPage int) (photos []Photo, paging paginator.Paging) {
    paging = paginator.Paginate(
        c,
        database.DB.Model(Photo{}),
        &photos,
        app.V1URL(database.TableName(&Photo{})),
        perPage,
    )
    return
}