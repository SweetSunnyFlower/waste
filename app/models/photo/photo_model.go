// Package photo 模型
package photo

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

type Photo struct {
	models.Model

	Name      string `gorm:"type:varchar(100);comment:名称;not null;" json:"name"`
	OriginURL string `gorm:"type:varchar(1000);comment:原始图片;not null;" json:"origin_url"`
	PsURL     string `gorm:"type:varchar(1000);comment:图片地址;not null;" json:"ps_url"`
	Remark    string `gorm:"type:varchar(1000);comment:备注;not null;" json:"remark"`
	DemoURL   string `gorm:"type:varchar(1000);comment:demo地址;not null;" json:"demo_url"`

	models.CommonTimestampsField
}

func (photo *Photo) Create() {
	database.DB.Create(&photo)
}

func (photo *Photo) Save() (rowsAffected int64) {
	result := database.DB.Save(&photo)
	return result.RowsAffected
}

func (photo *Photo) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&photo)
	return result.RowsAffected
}
