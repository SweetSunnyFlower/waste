// Package chat 模型
package chat

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

const (
	ChatTypeText = 1
	ChatTypeTrip = 2
)

type TripContent struct {
	ID     uint64 `gorm:"column:id;primary_key" json:"id"`
	Start  string `gorm:"column:start;not null" json:"start"`
	End    string `gorm:"column:end;not null" json:"end"`
	Time   string `gorm:"column:time;not null" json:"time"`
	Remark string `gorm:"column:remark;not null" json:"remark"`
	Price  uint   `gorm:"column:price;not null" json:"price"`
}

type Chat struct {
	models.Model

	UserID  uint64 `gorm:"column:user_id;not null" json:"user_id"`
	Type    int    `gorm:"column:type;not null" json:"type"`
	Content string `gorm:"column:content;not null" json:"content"`
	ToID    uint64 `gorm:"column:to_id;not null" json:"to_id"`

	User *User `gorm:"foreignkey:user_id;" json:"user"`

	models.CommonTimestampsField
}

type User struct {
	models.Model
	Nickname string `gorm:"column:name;comment:用户名;" json:"name"`
	Avatar   string `gorm:"column:avatar;comment:头像;" json:"avatar"`
}

func (chat *Chat) Create() {
	database.DB.Create(&chat)
}

func (chat *Chat) Save() (rowsAffected int64) {
	result := database.DB.Save(&chat)
	return result.RowsAffected
}

func (chat *Chat) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&chat)
	return result.RowsAffected
}
