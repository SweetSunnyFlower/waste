package user_passenger

import (
	"sweetsunnyflower/pkg/app"
	"sweetsunnyflower/pkg/database"
	"sweetsunnyflower/pkg/paginator"

	"github.com/gin-gonic/gin"
)

func Get(idstr string) (userPassenger UserPassenger) {
	database.DB.Where("id", idstr).First(&userPassenger)
	return
}

func GetBy(field, value string) (userPassenger UserPassenger) {
	database.DB.Where("? = ?", field, value).First(&userPassenger)
	return
}

func All() (userPassengers []UserPassenger) {
	database.DB.Find(&userPassengers)
	return
}

func IsExist(field, value string) bool {
	var count int64
	database.DB.Model(UserPassenger{}).Where("? = ?", field, value).Count(&count)
	return count > 0
}

func Paginate(c *gin.Context, perPage int) (userPassengers []UserPassenger, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(UserPassenger{}),
		&userPassengers,
		app.V1URL(database.TableName(&UserPassenger{})),
		perPage,
	)
	return
}

func GetUserPassengers(userID uint64) (userPassengers []UserPassenger) {
	database.DB.Where("user_id = ?", userID).Find(&userPassengers)
	return
}
