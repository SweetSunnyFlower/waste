// Package user_passenger 模型
package user_passenger

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

type UserPassenger struct {
	models.Model

	UserID uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	Name   string `gorm:"column:name;comment:姓名;index;" json:"name"`
	Phone  string `gorm:"column:phone;comment:手机号;index;" json:"phone"`

	models.CommonTimestampsField
}

func (userPassenger *UserPassenger) Create() {
	database.DB.Create(&userPassenger)
}

func (userPassenger *UserPassenger) Save() (rowsAffected int64) {
	result := database.DB.Save(&userPassenger)
	return result.RowsAffected
}

func (userPassenger *UserPassenger) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&userPassenger)
	return result.RowsAffected
}
