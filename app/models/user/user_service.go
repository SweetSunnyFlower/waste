package user

import (
	"sweetsunnyflower/pkg/app"
	"sweetsunnyflower/pkg/database"
	"sweetsunnyflower/pkg/paginator"

	"github.com/gin-gonic/gin"
)

// IsEmailExist 判断给定的邮箱地址是否已经存在于数据库中
// 参数 email：要判断的邮箱地址
// 返回值：若邮箱地址存在则返回 true，否则返回 false
func IsEmailExist(email string) bool {
	var count int64
	database.DB.Model(&User{}).Where("email = ?", email).Count(&count)
	return count > 0
}

// IsPhoneExist 判断给定的手机号是否已存在
// 参数phone: 待判断的手机号
// 返回值bool: 如果手机号已存在，返回true；否则返回false
func IsPhoneExist(phone string) bool {
	var count int64
	database.DB.Model(&User{}).Where("phone = ?", phone).Count(&count)
	return count > 0
}

// GetByPhone 通过手机号来获取用户
func GetByPhone(phone string) (userModel User) {
	database.DB.Where("phone = ?", phone).First(&userModel)
	return
}

// GetByMulti 通过 手机号/Email/用户名 来获取用户
func GetByMulti(loginID string) (userModel User) {
	database.DB.
		Where("phone = ?", loginID).
		Or("email = ?", loginID).
		Or("name = ?", loginID).
		First(&userModel)
	return
}

// Get 通过 ID 获取用户
func Get(idstr string) (userModel User) {
	database.DB.Where("id", idstr).First(&userModel)
	return
}

func GetById(id string) (userModel User) {
	database.DB.Where("id = ?", id).First(&userModel)
	return
}

// GetByEmail 通过 Email 来获取用户
func GetByEmail(email string) (userModel User) {
	database.DB.Where("email = ?", email).First(&userModel)
	return
}

// GetByOpenId 通过 openid 来获取用户
func GetByOpenId(openid string) (userModel User) {
	database.DB.Where("open_id = ?", openid).First(&userModel)
	return
}

func All() (users []User) {
	database.DB.Find(&users)
	return
}

// Create 创建用户，通过 User.ID 来判断是否创建成功
func Create(userModel User) User {
	database.DB.Create(&userModel)
	return userModel
}

// Paginate 分页内容
func Paginate(c *gin.Context, perPage int) (users []User, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(User{}),
		&users,
		app.V1URL(database.TableName(&User{})),
		perPage,
	)
	return
}
