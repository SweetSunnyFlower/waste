package user_blacklist

import (
	"sweetsunnyflower/pkg/app"
	"sweetsunnyflower/pkg/database"
	"sweetsunnyflower/pkg/paginator"

	"github.com/gin-gonic/gin"
)

func Get(idstr string) (userBlacklist UserBlacklist) {
	database.DB.Where("id", idstr).First(&userBlacklist)
	return
}

func GetBy(field, value string) (userBlacklist UserBlacklist) {
	database.DB.Where("? = ?", field, value).First(&userBlacklist)
	return
}

func All() (userBlacklists []UserBlacklist) {
	database.DB.Find(&userBlacklists)
	return
}

func IsExist(field, value string) bool {
	var count int64
	database.DB.Model(UserBlacklist{}).Where("? = ?", field, value).Count(&count)
	return count > 0
}

func Paginate(c *gin.Context, perPage int) (userBlacklists []UserBlacklist, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(UserBlacklist{}),
		&userBlacklists,
		app.V1URL(database.TableName(&UserBlacklist{})),
		perPage,
	)
	return
}

func GetUserBlacklists(userID uint64) (userBlacklists []UserBlacklist) {
	database.DB.Preload("User").Where("user_id = ?", userID).Find(&userBlacklists)
	return
}
