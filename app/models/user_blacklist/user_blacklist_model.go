// Package user_blacklist 模型
package user_blacklist

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

type UserBlacklist struct {
	models.Model

	UserID  uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	BlackID uint64 `gorm:"column:black_id;comment:黑名单用户ID;index;" json:"black_id"`
	User    *User  `gorm:"foreignkey:black_id;" json:"user"`

	models.CommonTimestampsField
}

type User struct {
	models.Model
	Name   string `gorm:"column:name;comment:用户名;" json:"name"`
	Avatar string `gorm:"column:avatar;comment:头像;" json:"avatar"`
}

func (userBlacklist *UserBlacklist) Create() {
	database.DB.Create(&userBlacklist)
}

func (userBlacklist *UserBlacklist) Save() (rowsAffected int64) {
	result := database.DB.Save(&userBlacklist)
	return result.RowsAffected
}

func (userBlacklist *UserBlacklist) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&userBlacklist)
	return result.RowsAffected
}
