// Package address 模型
package address

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

type Address struct {
	models.Model

	Name string `gorm:"type:varchar(255)" db:"name" json:"name" form:"name"`

	models.CommonTimestampsField
}

func (address *Address) Create() {
	database.DB.Create(&address)
}

func (address *Address) Save() (rowsAffected int64) {
	result := database.DB.Save(&address)
	return result.RowsAffected
}

func (address *Address) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&address)
	return result.RowsAffected
}
