package address

type ResponseAddress struct {
	ID       uint64             `json:"id"`
	Name     string             `json:"name"`
	Children []*ResponseAddress `json:"children,omitempty"`
}
