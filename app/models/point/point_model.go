// Package point 模型
package point

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

const (
	SignType    = 1 // 每日签到
	Certificate = 2 // 实名认证

	AddAction      = 1 // 加积分
	SubtractAction = 2 // 减积分
)

type Point struct {
	models.Model

	UserID      uint64 `gorm:"column:user_id;not null" json:"user_id"`
	Type        uint   `gorm:"column:type;not null" json:"type"`
	Action      uint   `gorm:"column:action;not null" json:"action"`       // 加还是减
	TargetID    uint64 `gorm:"column:target_id;not null" json:"target_id"` // 关联ID
	Value       int    `gorm:"column:value;not null" json:"value"`
	Description string `gorm:"column:description;not null" json:"description"`

	models.CommonTimestampsField
}

func (point *Point) Create() {
	database.DB.Create(&point)
}

func (point *Point) Save() (rowsAffected int64) {
	result := database.DB.Save(&point)
	return result.RowsAffected
}

func (point *Point) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&point)
	return result.RowsAffected
}
