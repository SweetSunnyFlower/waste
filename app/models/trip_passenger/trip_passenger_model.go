// Package trip_passenger 模型
package trip_passenger

import (
	"sweetsunnyflower/app/http/dto/address"
	"sweetsunnyflower/app/http/dto/car"
	"sweetsunnyflower/app/http/dto/user"
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

const PENDING = 1     // 待确认
const CONFIRMED = 2   // 已确认
const REFUSE = 3      // 已拒绝
const CANCEL = 4      // 已取消
const CANCELLED = 5   // 被取消
const FINISH = 6      // 已完成
const AUTO_CANCEL = 7 // 自动取消,司机发车或者乘客已满，自动将其他乘客的状态转换为取消

type TripPassenger struct {
	models.Model

	TripID    uint64     `gorm:"column:trip_id;comment:行程ID;index;" json:"trip_id"`
	UserID    uint64     `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	Status    uint8      `gorm:"column:status;comment:状态;default:1;" json:"status"`
	Passenger string     `gorm:"type:text;column:passenger;comment:乘客信息;" json:"passenger"`
	Trip      string     `gorm:"type:text;column:trip;comment:行程信息;" json:"trip"`
	Booking   string     `gorm:"type:text;column:booking;comment:预约信息;" json:"booking"`
	User      *user.User `gorm:"foreignKey:user_id;" json:"user"`

	models.CommonTimestampsField
}

type Booking struct {
	Sites uint `json:"sites"`
	Price uint `json:"price"`
}

type Passenger struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
}

type Trip struct {
	ID      uint64           `json:"id"`
	UserID  uint64           `json:"user_id"`
	StartID uint64           `json:"start_id"`
	EndID   uint64           `json:"end_id"`
	Time    string           `json:"time"`
	CarId   uint64           `json:"car_id"`
	Sites   uint             `json:"sites"`
	Price   uint             `json:"price"`
	Status  uint             `json:"status"`
	Remark  string           `json:"remark"`
	Line    string           `json:"line"`
	Start   *address.Address `gorm:"foreignKey:start_id;" json:"start"`
	End     *address.Address `gorm:"foreignKey:end_id;" json:"end"`
	Car     *car.UserCar     `gorm:"foreignKey:car_id;" json:"car"`
}

func (tripPassenger *TripPassenger) Create() {
	database.DB.Create(&tripPassenger)
}

func (tripPassenger *TripPassenger) Save() (rowsAffected int64) {
	result := database.DB.Save(&tripPassenger)
	return result.RowsAffected
}

func (tripPassenger *TripPassenger) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&tripPassenger)
	return result.RowsAffected
}
