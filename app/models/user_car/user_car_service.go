package user_car

import (
	"sweetsunnyflower/pkg/app"
	"sweetsunnyflower/pkg/database"
	"sweetsunnyflower/pkg/paginator"

	"github.com/gin-gonic/gin"
)

func Get(idstr string) (userCar UserCar) {
	database.DB.Where("id", idstr).First(&userCar)
	return
}

func GetBy(field, value string) (userCar UserCar) {
	database.DB.Where("? = ?", field, value).First(&userCar)
	return
}

func All() (userCars []UserCar) {
	database.DB.Find(&userCars)
	return
}

func IsExist(field, value string) bool {
	var count int64
	database.DB.Model(UserCar{}).Where("? = ?", field, value).Count(&count)
	return count > 0
}

func Paginate(c *gin.Context, perPage int) (userCars []UserCar, paging paginator.Paging) {
	paging = paginator.Paginate(
		c,
		database.DB.Model(UserCar{}),
		&userCars,
		app.V1URL(database.TableName(&UserCar{})),
		perPage,
	)
	return
}

func GetUserCars(userID uint64) (userCar []UserCar) {
	database.DB.Where("user_id = ?", userID).Find(&userCar)
	return
}
