// Package user_car 模型
package user_car

import (
	"sweetsunnyflower/app/models"
	"sweetsunnyflower/pkg/database"
)

type UserCar struct {
	models.Model

	Brand    string `gorm:"column:brand;type:varchar(255);comment:品牌;" json:"brand"`
	Color    string `gorm:"column:color;type:varchar(255);comment:颜色;" json:"color"`
	Plate    string `gorm:"column:plate;type:varchar(255);comment:车牌;" json:"plate"`
	Sites    uint   `gorm:"column:sites;type:int(10);comment:座位数;" json:"sites"`
	UserID   uint64 `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	UseYears uint   `gorm:"column:use_years;type:int(10);comment:使用年限;" json:"use_years"`

	models.CommonTimestampsField
}

func (userCar *UserCar) Create() {
	database.DB.Create(&userCar)
}

func (userCar *UserCar) Save() (rowsAffected int64) {
	result := database.DB.Save(&userCar)
	return result.RowsAffected
}

func (userCar *UserCar) Delete() (rowsAffected int64) {
	result := database.DB.Delete(&userCar)
	return result.RowsAffected
}
