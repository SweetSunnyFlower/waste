package queues

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/hibiken/asynq"
)

type ExamplePayload struct {
	Phone string
}

const Example = "example"

func HandleExampleTask(ctx context.Context, t *asynq.Task) error {
	var p ExamplePayload
	if err := json.Unmarshal(t.Payload(), &p); err != nil {
		return fmt.Errorf("json.Unmarshal failed: %v: %w", err, asynq.SkipRetry)
	}
	fmt.Println("HandleExampleTask:", p.Phone)

	// Email delivery code ...
	return nil
}

var Tasks map[string]func(ctx context.Context, t *asynq.Task) error = map[string]func(ctx context.Context, t *asynq.Task) error{
	Example:  HandleExampleTask,
	Publish:  HandlePublishTask,
	Register: HandleRegisterTask,
	Sign:     HandleSignTask,
}
