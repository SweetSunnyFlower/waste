package v1

import (
	"sweetsunnyflower/app/requests"
	"sweetsunnyflower/pkg/config"
	"sweetsunnyflower/pkg/file"
	"sweetsunnyflower/pkg/response"

	"github.com/gin-gonic/gin"
)

type CommonController struct {
	Controller
}

func (ctrl *CommonController) UploadImage(c *gin.Context) {
	request := requests.UploadImageRequest{}
	if ok := requests.Validate(c, &request, requests.UploadImage); !ok {
		return
	}
	imageUrl, err := file.SaveUploadImage(c, request.Image)
	if err != nil {
		response.Abort500(c, "上传图片失败，请稍后尝试~")
		return
	}

	response.Data(c, map[string]string{
		"image_url": config.GetString("app.url") + imageUrl,
		"name":      request.Image.Filename,
	})
}
