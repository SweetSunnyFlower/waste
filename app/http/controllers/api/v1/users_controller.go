package v1

import (
	"errors"
	dtouser "sweetsunnyflower/app/http/dto/user"
	"sweetsunnyflower/app/models/user"
	"sweetsunnyflower/app/models/user_certification"
	"sweetsunnyflower/app/models/user_trip"
	"sweetsunnyflower/pkg/auth"
	"sweetsunnyflower/pkg/config"
	"sweetsunnyflower/pkg/file"

	"sweetsunnyflower/app/requests"
	"sweetsunnyflower/pkg/response"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

type UsersController struct {
	Controller
}

func (ctrl *UsersController) UpdateAvatar(c *gin.Context) {

	request := requests.UserUpdateAvatarRequest{}
	if ok := requests.Validate(c, &request, requests.UserUpdateAvatar); !ok {
		return
	}

	avatar, err := file.SaveUploadAvatar(c, request.Avatar)
	if err != nil {
		response.Abort500(c, "上传头像失败，请稍后尝试~")
		return
	}

	currentUser := auth.CurrentUser(c)
	currentUser.Avatar = config.GetString("app.url") + avatar
	currentUser.Save()

	response.Data(c, currentUser)
}

// CurrentUser 当前登录用户信息
func (ctrl *UsersController) CurrentUser(c *gin.Context) {
	userModel := auth.CurrentUser(c)

	user := dtouser.User{
		ID:            userModel.ID,
		Nickname:      userModel.Nickname,
		Certification: userModel.Certification,
		Avatar:        userModel.Avatar,
		Gender:        userModel.Gender,
		Age:           userModel.Age,
		Work:          userModel.Work,
		Phone:         userModel.Phone,
		Name:          userModel.Name,
		Follows:       userModel.Follows,
		Blacks:        userModel.Blacks,
		Point:         userModel.Point,
		Trips:         cast.ToUint64(user_trip.Count(requests.UserTripsQueryRequest{UserID: auth.CurrentIntUID(c)})),
	}

	response.Data(c, user)
}

// Index 所有用户
func (ctrl *UsersController) Index(c *gin.Context) {
	request := requests.PaginationRequest{}
	if ok := requests.Validate(c, &request, requests.Pagination); !ok {
		return
	}

	data, pager := user.Paginate(c, 10)
	response.JSON(c, gin.H{
		"data":  data,
		"pager": pager,
	})
}

func (ctrl *UsersController) UpdatePhone(c *gin.Context) {
	request := requests.UpdatePhoneRequest{}
	if ok := requests.Validate(c, &request, requests.UpdatePhone); !ok {
		return
	}

	userModel := auth.CurrentUser(c)
	userModel.Phone = request.Phone
	userModel.Save()
	response.Data(c, userModel)
}

func (ctrl *UsersController) UpdateNickname(c *gin.Context) {
	request := requests.UpdateNicknameRequest{}
	if ok := requests.Validate(c, &request, requests.UpdateNickname); !ok {
		return
	}

	userModel := auth.CurrentUser(c)
	userModel.Nickname = request.Nickname
	userModel.Save()
	response.Data(c, userModel)
}

func (ctrl *UsersController) UpdateWork(c *gin.Context) {
	request := requests.UpdateWorkRequest{}
	if ok := requests.Validate(c, &request, requests.UpdateWork); !ok {
		return
	}

	userModel := auth.CurrentUser(c)
	userModel.Work = request.Work
	userModel.Save()
	response.Data(c, userModel)
}

func (ctrl *UsersController) UpdateGender(c *gin.Context) {
	request := requests.UpdateGenderRequest{}
	if ok := requests.Validate(c, &request, requests.UpdateGender); !ok {
		return
	}

	userModel := auth.CurrentUser(c)
	userModel.Gender = request.Gender
	userModel.Save()
	response.Data(c, userModel)
}

func (ctrl *UsersController) UpdateAge(c *gin.Context) {
	request := requests.UpdateAgeRequest{}
	if ok := requests.Validate(c, &request, requests.UpdateAge); !ok {
		return
	}

	userModel := auth.CurrentUser(c)
	userModel.Age = request.Age
	userModel.Save()
	response.Data(c, userModel)
}

// 实名认证
func (ctrl *UsersController) Certification(c *gin.Context) {
	request := requests.CertificationRequest{}
	if ok := requests.Validate(c, &request, requests.Certification); !ok {
		return
	}

	userModel := auth.CurrentUser(c)

	latestCertification := user_certification.GetUserLatestCertification(cast.ToString(userModel.ID))

	if latestCertification != nil {
		if latestCertification.Status == user_certification.PASS {
			response.Error(c, errors.New("您已通过认证，无需重复提交"))
			return
		}
		if latestCertification.Status == user_certification.PENDING {
			response.Error(c, errors.New("您已提交过认证申请，请耐心等待审核"))
			return
		}
	}

	certificationModel := &user_certification.UserCertification{
		UserID:      userModel.ID,
		Name:        request.Name,
		IdCard:      request.IdCard,
		IdCardFront: request.IdCardFront,
		IdCardBack:  request.IdCardBack,
		Status:      user_certification.PENDING,
	}

	certificationModel.Create()

	response.Data(c, certificationModel)
}

// 最后一次实名认证信息
func (ctrl *UsersController) LatestCertification(c *gin.Context) {

	userModel := auth.CurrentUser(c)

	latestCertification := user_certification.GetUserLatestCertification(cast.ToString(userModel.ID))

	response.Data(c, latestCertification)
}

// 优秀司机
// func (ctrl *UsersController) Excellent
