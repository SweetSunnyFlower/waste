package v1

import (
	"sweetsunnyflower/app/models/point"
	"sweetsunnyflower/app/requests"
	"sweetsunnyflower/pkg/auth"
	"sweetsunnyflower/pkg/response"

	"github.com/gin-gonic/gin"
)

type PointsController struct {
	Controller
}

func (ctrl *PointsController) Index(c *gin.Context) {

	request := requests.PointQueryRequest{}
	if ok := requests.Validate(c, &request, requests.PointQuery); !ok {
		return
	}

	request.UserID = auth.CurrentIntUID(c)

	points := point.GetByLatestId(request)
	count := point.Count(request)
	sum := point.Sum(request)

	response.Data(c, gin.H{"points": points, "total": count, "sum": sum})
}
