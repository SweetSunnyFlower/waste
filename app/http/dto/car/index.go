package car

type UserCar struct {
	ID    uint64 `json:"id"`
	Brand string `json:"brand"`
	Sites uint   `json:"sites"`
	Plate string `json:"plate"`
	Color string `json:"color"`
}
