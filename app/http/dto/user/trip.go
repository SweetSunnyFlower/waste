package user

import (
	"sweetsunnyflower/app/http/dto/address"
	"sweetsunnyflower/app/http/dto/car"
	"sweetsunnyflower/app/models"
)

type UserTrip struct {
	ID uint64 `json:"id"`

	UserID    uint64           `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	StartID   uint64           `gorm:"column:start_id;comment:起点ID;index;" json:"start_id"`
	EndID     uint64           `gorm:"column:end_id;comment:终点ID;index;" json:"end_id"`
	Time      string           `gorm:"column:time;comment:起点时间;index;" json:"time"`
	CarId     uint64           `gorm:"column:car_id;comment:车辆ID;index;" json:"car_id"`
	Sites     uint             `gorm:"column:sites;comment:座位数量;index;" json:"sites"`
	Remaining uint             `gorm:"column:remaining;comment:剩余座位;index;" json:"remaining"`
	Price     uint             `gorm:"column:price;comment:价格;index;" json:"price"`
	Status    uint             `gorm:"column:status;comment:状态;index;" json:"status"`
	Remark    string           `gorm:"column:remark;comment:备注;" json:"remark"`
	Line      string           `gorm:"column:line;comment:线路;" json:"line"`
	Start     *address.Address `gorm:"foreignKey:start_id;" json:"start"`
	End       *address.Address `gorm:"foreignKey:end_id;" json:"end"`
	Car       *car.UserCar     `gorm:"foreignKey:car_id;" json:"car"`
	User      *User            `gorm:"foreignKey:user_id;" json:"user"`
	Follow    bool             `gorm:"column:follow;comment:是否关注;index;" json:"follow"`

	TripPassenger []*TripPassenger  `gorm:"foreignKey:trip_id;" json:"passengers,omitempty"`
	CreatedAt     models.FormatTime `gorm:"default:CURRENT_TIMESTAMP;column:created_at;comment:创建时间;type:timestamp;index;" json:"created_at"  form:"created_at"  db:"created_at"`
	UpdatedAt     models.FormatTime `gorm:"default:CURRENT_TIMESTAMP;column:updated_at;comment:更新时间;type:timestamp;index;" json:"updated_at"  form:"updated_at"  db:"updated_at"`
}
