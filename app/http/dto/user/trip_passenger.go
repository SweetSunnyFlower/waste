package user

import (
	"sweetsunnyflower/app/http/dto/address"
	"sweetsunnyflower/app/http/dto/car"
	"sweetsunnyflower/app/models"
)

type TripPassenger struct {
	ID uint64 `json:"id"`

	TripID    uint64            `gorm:"column:trip_id;comment:行程ID;index;" json:"trip_id"`
	UserID    uint64            `gorm:"column:user_id;comment:用户ID;index;" json:"user_id"`
	Status    uint8             `gorm:"column:status;comment:状态;default:1;" json:"status"`
	Passenger Passenger         `gorm:"type:text;column:passenger;comment:乘客信息;" json:"passenger"`
	Trip      Trip              `gorm:"type:text;column:trip;comment:行程信息;" json:"trip"`
	Booking   Booking           `gorm:"type:text;column:booking;comment:预约信息;" json:"booking"`
	User      *User             `gorm:"foreignKey:user_id;" json:"user,omitempty"`
	CreatedAt models.FormatTime `gorm:"default:CURRENT_TIMESTAMP;column:created_at;comment:创建时间;type:timestamp;index;" json:"created_at"  form:"created_at"  db:"created_at"`
}

type Booking struct {
	Sites uint `json:"sites"`
	Price uint `json:"price"`
}

type Passenger struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
}

type Trip struct {
	ID      uint64           `json:"id"`
	UserID  uint64           `json:"user_id"`
	StartID uint64           `json:"start_id"`
	EndID   uint64           `json:"end_id"`
	Time    string           `json:"time"`
	CarId   uint64           `json:"car_id"`
	Sites   uint             `json:"sites"`
	Price   uint             `json:"price"`
	Status  uint             `json:"status"`
	Remark  string           `json:"remark"`
	Line    string           `json:"line"`
	Start   *address.Address `gorm:"foreignKey:start_id;" json:"start"`
	End     *address.Address `gorm:"foreignKey:end_id;" json:"end"`
	Car     *car.UserCar     `gorm:"foreignKey:car_id;" json:"car"`
}
