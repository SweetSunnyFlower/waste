package user

type User struct {
	ID            uint64 `json:"id"`
	Name          string `gorm:"type:varchar(255);comment:用户真实姓名;not null;index" json:"name"`
	Nickname      string `gorm:"type:varchar(255);comment:用户昵称;not null" json:"nickname"`
	Avatar        string `gorm:"type:varchar(255);comment:用户头像;not null;" json:"avatar"`
	Phone         string `gorm:"type:varchar(20);comment:手机号;index;default:null" json:"phone"`
	Age           string `gorm:"type:varchar(255);comment:年龄;default:null" json:"age"`
	Work          string `gorm:"type:varchar(255);comment:工作;default:null" json:"work"`
	Gender        uint8  `gorm:"comment:性别 2:女,1:男;default:0;" db:"gender" json:"gender" form:"gender"`
	Follows       uint64 `gorm:"comment:关注数;default:0" json:"follows"`
	Blacks        uint64 `gorm:"comment:黑名单数;default:0" json:"blacks"`
	Trips         uint64 `gorm:"comment:行程数;default:0" json:"trips"`
	Certification uint8  `gorm:"type:tinyint;comment:是否认证;default:null" json:"certification"`
	Balance       uint64 `gorm:"comment:余额;default:0" json:"balance"`
	Point         uint64 `gorm:"comment:积分;default:0" json:"point"`
}
