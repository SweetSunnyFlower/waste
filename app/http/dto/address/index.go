package address

type Address struct {
	ID   uint64 `json:"id"`
	Name string `json:"name"`
}
