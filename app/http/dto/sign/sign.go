package sign

import (
	"sweetsunnyflower/app/models/sign"

	"github.com/golang-module/carbon/v2"
)

type SignResponse struct {
	Week   int  `gorm:"column:week;not null" json:"week"`
	Signed bool `gorm:"column:signed;not null" json:"signed"`
	Today  bool `gorm:"column:today;not null" json:"today"`
}

func NewSignResponse(sign *sign.Sign) *SignResponse {
	dayOfWeek := carbon.Now().DayOfWeek()
	return &SignResponse{
		Week:   sign.Week,
		Signed: sign.ID > 0,
		Today:  dayOfWeek == sign.Week+1,
	}
}
