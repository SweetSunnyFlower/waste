package notify

import (
	"encoding/json"
	"sweetsunnyflower/app/models"
)

func UnmarshalContent(data []byte) (Content, error) {
	var r Content
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Content) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Content struct {
	ID        uint64            `json:"id"`
	Type      uint64            `json:"type"`
	Content   ContentClass      `json:"content"`
	Status    uint8             `json:"status"`
	DeletedAt interface{}       `json:"deleted_at"`
	CreatedAt models.FormatTime `json:"created_at"`
}

type ContentClass struct {
	Icon    string `json:"icon"`
	Title   string `json:"title"`
	Message string `json:"message"`
	Target  string `json:"target"`
}
