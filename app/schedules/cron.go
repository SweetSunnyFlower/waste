package schedules

import (
	"sweetsunnyflower/pkg/logger"
	// "sweetsunnyflower/pkg/schedule"
)

// see https://github.com/jasonlvhit/gocron/blob/master/README.md
func Register() {
	logger.Info("schedules, running!")
	// schedule.CronInstance.Cron.Every(20).Seconds().Do(Example)
}

func Example() {
	logger.Info("schedules, running!")
}
