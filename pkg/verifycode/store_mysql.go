package verifycode

import (
	"sweetsunnyflower/app/models/verifycode"
	"sweetsunnyflower/pkg/app"
	"sweetsunnyflower/pkg/config"
	"time"
)

type MysqlStore struct {
	MysqlClient *verifycode.VerifyCode
	KeyPrefix   string
}

// Set 实现 verifycode.Store interface 的 Set 方法
func (s *MysqlStore) Set(key string, value string) bool {

	ExpireTime := time.Minute * time.Duration(config.GetInt64("verifycode.expire_time"))
	// 本地环境方便调试
	if app.IsLocal() {
		ExpireTime = time.Minute * time.Duration(config.GetInt64("verifycode.debug_expire_time"))
	}

	return s.MysqlClient.Set(s.KeyPrefix+key, value, ExpireTime)
}

// Get 实现 verifycode.Store interface 的 Get 方法
func (s *MysqlStore) Get(key string, clear bool) (value string) {
	key = s.KeyPrefix + key
	val := s.MysqlClient.Get(key)
	if clear {
		s.MysqlClient.Del(key)
	}
	return val
}

// Verify 实现 verifycode.Store interface 的 Verify 方法
func (s *MysqlStore) Verify(key, answer string, clear bool) bool {
	v := s.Get(key, clear)
	return v == answer
}
