package schedule

import (
	"sync"

	"github.com/jasonlvhit/gocron"
)

// see https://github.com/jasonlvhit/gocron/blob/master/README.md

var once sync.Once

var CronInstance *Cron

type Cron struct {
	Cron *gocron.Scheduler
}

func NewCron() *Cron {
	once.Do(func() {
		CronInstance = &Cron{
			Cron: gocron.NewScheduler(),
		}
	})

	return CronInstance
}

func (c *Cron) Start() {
	<-c.Cron.Start()
}
