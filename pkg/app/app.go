package app

import (
	"sweetsunnyflower/pkg/config"
	"time"
)

// IsLocal 判断当前环境是否为本地环境
// 返回值为bool类型，true表示是本地环境，false表示不是本地环境
func IsLocal() bool {
	return config.Get("app.env") == "local"
}

// IsProduction 判断当前环境是否为生产环境
// 返回值为bool类型，若为生产环境则返回true，否则返回false
func IsProduction() bool {
	return config.Get("app.env") == "production"
}

// IsTesting 函数判断当前环境是否为"testing"。
// 如果当前环境为"testing"，则返回true；否则返回false。
func IsTesting() bool {
	return config.Get("app.env") == "testing"
}

// TimenowInTimezone 获取当前时间，支持时区
func TimenowInTimezone() time.Time {
	chinaTimezone, _ := time.LoadLocation(config.GetString("app.timezone"))
	return time.Now().In(chinaTimezone)
}

// URL 传参 path 拼接站点的 URL
func URL(path string) string {
	return config.Get("app.url") + path
}

// V1URL 拼接带 v1 标示 URL
func V1URL(path string) string {
	return URL("/v1/" + path)
}
