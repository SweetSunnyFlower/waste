package wechat

import (
	"sweetsunnyflower/pkg/config"
	"sync"

	"github.com/ArtisanCloud/PowerWeChat/v3/src/officialAccount"
)

type Wechat struct {
	OfficialAccount *officialAccount.OfficialAccount
}

var WechatInstance *Wechat

var once sync.Once

func NewWechat() *Wechat {
	once.Do(func() {
		officialAccount, _ := officialAccount.NewOfficialAccount(&officialAccount.UserConfig{
			AppID:  config.GetString("wechat.office.app_id"),     // 公众号、小程序的appid
			Secret: config.GetString("wechat.office.app_secret"), //

			Token:  config.GetString("wechat.office.token"),
			AESKey: config.GetString("wechat.office.aes_key"),

			Log: officialAccount.Log{
				Level: "debug",
				File:  "./wechat.log",
			},

			HttpDebug: true,
			Debug:     false,
		})
		WechatInstance = &Wechat{
			OfficialAccount: officialAccount,
		}
	})
	return WechatInstance
}
