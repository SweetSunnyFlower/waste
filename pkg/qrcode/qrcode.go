package qrcode

import (
	"encoding/base64"

	qrcode "github.com/skip2/go-qrcode"
)

// CreateQrCode 生成二维码
//
// 参数：
// content string - 二维码内容
// output string - 二维码输出路径，如果为空则返回二维码的base64编码字符串
//
// 返回值：
// string - 二维码的base64编码字符串，或者输出路径
// error - 错误信息，如果生成二维码过程中出现错误则返回非空错误信息
func CreateQrCode(content string, output string) (string, error) {

	if output != "" {

		err := qrcode.WriteFile(content, qrcode.Highest, 256, output)
		if err != nil {
			return "", err
		}

		return output, nil

	} else {
		png, err := qrcode.Encode(content, qrcode.Highest, 256)

		if err != nil {
			return "", err
		}
		str := "data:image/png;base64," + base64.StdEncoding.EncodeToString(png)
		return str, nil
	}
}
